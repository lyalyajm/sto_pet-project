package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.enumeration.WorkType;
import kz.lyalya.sto.common_service.impl.done_work.DoneWorkServiceImpl;
import kz.lyalya.sto.dto.done_work.DoneWorkCreateDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DoneWorkServiceTest {
    private final long REQUEST_ID = 6;
    private final long ID = 1;

    @Autowired
    private DoneWorkServiceImpl doneWorkService;

    @Test
    @Order(1)
    @Sql(value = {"/sql/users_roles.sql", "/sql/done_works_test.sql"})
    public void shouldAdd() {
        DoneWorkCreateDTO dto = new DoneWorkCreateDTO();
        dto.setRequestId(REQUEST_ID);
        dto.setWorkType(WorkType.BELT.name());
        dto.setNote("Note");
        dto.setPrice(50000);
        dto.setDate(LocalDate.now());

        DoneWorkDTO result = doneWorkService.add(dto);

        Assertions.assertNotNull(result);
    }

    @Test
    @Order(2)
    public void shouldUpdate() {
        DoneWorkUpdateDTO dto = new DoneWorkUpdateDTO();
        dto.setId(ID);
        dto.setWorkType(WorkType.ENGINE.name());
        dto.setNote("Updated");
        dto.setPrice(100000);
        dto.setDate(LocalDate.now());

        DoneWorkDTO result = doneWorkService.update(dto);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getWorkType(), WorkType.ENGINE);
        Assertions.assertEquals(result.getNote(), "Updated");
        Assertions.assertEquals(result.getPrice(), 100000);
    }

    @Test
    @Order(3)
    public void shouldDelete() {
        doneWorkService.delete(ID);

        Assertions.assertThrows(EntityNotFoundException.class, () -> doneWorkService.findOne(ID));
    }
}
