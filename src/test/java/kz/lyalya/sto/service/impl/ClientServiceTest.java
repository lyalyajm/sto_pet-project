package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_service.impl.user.ClientServiceImpl;
import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;

@SpringBootTest
@Sql({"/sql/users_roles.sql"})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientServiceTest {
    private final long ID = 1;
    private final String CLIENT_EMAIL = "client-test1@mail.ru";
    private final String PASSWORD = "test";
    private final String UPDATED_PASSWORD = "test2";

    @Autowired
    private ClientServiceImpl clientService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    @Order(1)
    public void shouldAdd() {
        ClientCreateDTO dto = new ClientCreateDTO();
        dto.setEmail(CLIENT_EMAIL);
        dto.setName("client-test");
        dto.setSurname("client-test");
        dto.setDateOfBirth(LocalDate.of(1999, 5, 5));
        dto.setPhoneNumber("+77778889944");
        dto.setPassword(PASSWORD);

        ClientDTO result = clientService.add(dto);

        Assertions.assertNotNull(result);
    }

    @Test
    @Order(2)
    public void shouldFindOneById() {
        Client client = clientService.findOne(ID);

        Assertions.assertNotNull(client);
        Assertions.assertEquals(client.getUser().getEmail(), CLIENT_EMAIL);
    }

    @Test
    @Order(3)
    public void shouldFindOneByEmail() {
        Client client = clientService.findOne(CLIENT_EMAIL);

        Assertions.assertNotNull(client);
        Assertions.assertEquals(client.getUser().getId(), ID);
    }

    @Test
    @Order(4)
    public void shouldUpdateClientPassword() {
        UserPasswordUpdateDTO dto = new UserPasswordUpdateDTO(PASSWORD, UPDATED_PASSWORD);
        clientService.updatePassword(dto, ID, CLIENT_EMAIL);
        Client client = clientService.findOne(ID);

        Assertions.assertTrue(passwordEncoder.matches(UPDATED_PASSWORD, client.getUser().getPassword()));
    }

    @Test
    @Order(5)
    public void shouldUpdate() {
        Client client = clientService.findOne(ID);
        String newName = client.getUser().getName() + "updated";
        String newSurname = client.getUser().getSurname() + "updated";
        String newEmail = "update" + CLIENT_EMAIL;

        ClientUpdateDTO dto = new ClientUpdateDTO();
        dto.setId(ID);
        dto.setName(newName);
        dto.setSurname(newSurname);
        dto.setEmail(newEmail);
        dto.setDateOfBirth(client.getUser().getDateOfBirth());
        dto.setPhoneNumber(client.getUser().getPhoneNumber());

        ClientDTO result = clientService.update(dto, CLIENT_EMAIL);

        Assertions.assertEquals(result.getEmail(), newEmail);
        Assertions.assertEquals(result.getName(), newName);
        Assertions.assertEquals(result.getSurname(), newSurname);
    }

    @Test
    @Order(6)
    public void shouldDelete() {
        boolean result = clientService.delete(ID);

        Assertions.assertTrue(result);
    }
}
