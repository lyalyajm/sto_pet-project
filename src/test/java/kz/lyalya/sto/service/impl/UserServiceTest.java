package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.repository.UserRepository;
import kz.lyalya.sto.common_service.impl.user.UserServiceImpl;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql({"/sql/users_roles.sql", "/sql/users_test.sql"})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserServiceTest {
    private final long ID = 1;
    private final String EMAIL = "test1@mail.ru";
    private final String PASSWORD = "test";
    private final String UPDATED_PASSWORD = "test2";

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    @Order(1)
    public void shouldFindUserById() {
        User user = userService.findOne(ID);

        Assertions.assertNotNull(user);
        Assertions.assertEquals(user.getEmail(), EMAIL);
    }

    @Test
    @Order(2)
    public void shouldFindUserByEmail() {
        User user = userService.findOne(EMAIL);

        Assertions.assertNotNull(user);
        Assertions.assertEquals(user.getId(), ID);
    }

    @Test
    @Order(3)
    public void shouldUpdateUserPassword() {
        UserPasswordUpdateDTO dto = new UserPasswordUpdateDTO(PASSWORD, UPDATED_PASSWORD);
        userService.updatePassword(dto, ID);

        Assertions.assertTrue(passwordEncoder.matches(UPDATED_PASSWORD, userService.findOne(ID).getPassword()));
    }

    @Test
    @Order(4)
    public void shouldDeleteUser() {
        userService.delete(ID);

        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.findOne(ID));
    }
}
