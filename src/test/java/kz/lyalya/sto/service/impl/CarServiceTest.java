package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.enumeration.CarMark;
import kz.lyalya.sto.common_data.enumeration.CarState;
import kz.lyalya.sto.common_service.impl.car.CarServiceImpl;
import kz.lyalya.sto.dto.car.CarCreateDTO;
import kz.lyalya.sto.dto.car.CarDTO;
import kz.lyalya.sto.dto.car.CarUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CarServiceTest {
    private final String OWNER_EMAIL = "client@mail.ru";
    private final long ID = 1;

    @Autowired
    private CarServiceImpl carService;

    @Test
    @Order(1)
    @Sql("/sql/cars_test.sql")
    public void shouldAdd() {
        CarCreateDTO dto = new CarCreateDTO();
        dto.setMark(CarMark.Audi);
        dto.setModel("A8");
        dto.setYearOfProduce(2022);

        CarDTO result = carService.add(dto, OWNER_EMAIL);

        Assertions.assertNotNull(result);
    }

    @Test
    @Order(2)
    public void shouldFindOneById() {
        Car car = carService.findOne(ID);

        Assertions.assertNotNull(car);
        Assertions.assertEquals(car.getOwner().getUser().getEmail(), OWNER_EMAIL);
    }

    @Test
    @Order(3)
    public void shouldChangeState() {
        CarState state = CarState.REPAIRING;
        Car car = carService.changeState(ID, state);

        Assertions.assertEquals(car.getState(), state);
        carService.changeState(ID, CarState.NEW);
    }

    @Test
    @Order(4)
    public void shouldUpdate() {
        CarUpdateDTO dto = new CarUpdateDTO();
        dto.setId(ID);
        dto.setMark(CarMark.BMW);
        dto.setModel("x5");
        dto.setYearOfProduce(2020);

        CarDTO result = carService.update(dto);

        Assertions.assertEquals(CarMark.valueOf(result.getMark()), dto.getMark());
        Assertions.assertEquals(result.getModel(), dto.getModel());
        Assertions.assertEquals(result.getYearOfProduce(), dto.getYearOfProduce());
    }

    @Test
    @Order(5)
    public void shouldDelete() {
        carService.delete(ID);

        Assertions.assertThrows(EntityNotFoundException.class, () -> carService.findOne(ID));
    }
}
