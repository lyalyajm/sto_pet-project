package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_service.impl.user.ClientServiceImpl;
import kz.lyalya.sto.common_service.impl.user.MasterServiceImpl;
import kz.lyalya.sto.common_service.impl.user.UserServiceImpl;
import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.dto.master.MasterCreateDTO;
import kz.lyalya.sto.dto.master.MasterDTO;
import kz.lyalya.sto.dto.master.MasterUpdateDTO;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MasterServiceTest {
    private final long ID = 1;
    private final String MASTER_EMAIL = "master-test1@mail.ru";
    private final String ADMIN_EMAIL = "admin@mail.ru";
    private final String PASSWORD = "test";
    private final String UPDATED_PASSWORD = "test2";

    @Autowired
    private MasterServiceImpl masterService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    @Order(1)
    @Sql(value = {"/sql/users_roles.sql", "/sql/masters_test.sql"})
    public void shouldAdd() {

        short experience = 3;

        MasterCreateDTO dto = new MasterCreateDTO();
        dto.setEmail(MASTER_EMAIL);
        dto.setName("master-test");
        dto.setSurname("master-test");
        dto.setDateOfBirth(LocalDate.of(1999, 5, 5));
        dto.setPhoneNumber("+77778889944");
        dto.setPassword(PASSWORD);
        dto.setExperience(experience);
        dto.setDescription("Master");

        MasterDTO result = masterService.add(dto, ADMIN_EMAIL);

        Assertions.assertNotNull(result);
    }

    @Test
    @Order(2)
    public void shouldFindOneById() {
        Master master = masterService.findOne(ID);

        Assertions.assertNotNull(master);
        Assertions.assertEquals(master.getUser().getEmail(), MASTER_EMAIL);
    }

    @Test
    @Order(3)
    public void shouldFindOneByEmail() {
        Master master = masterService.findOne(ID);

        Assertions.assertNotNull(master);
        Assertions.assertEquals(master.getUser().getId(), ID);
    }

    @Test
    @Order(4)
    public void shouldUpdateMasterPassword() {
        UserPasswordUpdateDTO dto = new UserPasswordUpdateDTO(PASSWORD, UPDATED_PASSWORD);
        masterService.updatePassword(dto, ID);
        Master master = masterService.findOne(ID);

        Assertions.assertTrue(passwordEncoder.matches(UPDATED_PASSWORD, master.getUser().getPassword()));
    }

    @Test
    @Order(5)
    public void shouldUpdate() {
        Master master = masterService.findOne(ID);

        String newName = master.getUser().getName() + "updated";
        String newSurname = master.getUser().getSurname() + "updated";
        String newEmail = "update" + MASTER_EMAIL;

        MasterUpdateDTO dto = new MasterUpdateDTO();
        dto.setId(ID);
        dto.setName(newName);
        dto.setSurname(newSurname);
        dto.setEmail(newEmail);
        dto.setDateOfBirth(master.getUser().getDateOfBirth());
        dto.setPhoneNumber(master.getUser().getPhoneNumber());
        dto.setExperience(master.getExperience());
        dto.setDescription(master.getDescription());

        MasterDTO result = masterService.update(dto);

        Assertions.assertEquals(result.getEmail(), newEmail);
        Assertions.assertEquals(result.getName(), newName);
        Assertions.assertEquals(result.getSurname(), newSurname);
    }

    @Test
    @Order(6)
    public void shouldDelete() {
        boolean result = masterService.delete(ID);

        Assertions.assertTrue(result);
    }
}

