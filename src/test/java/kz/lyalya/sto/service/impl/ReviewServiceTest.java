package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.common_service.impl.request.RequestServiceImpl;
import kz.lyalya.sto.common_service.impl.review.ReviewServiceImpl;
import kz.lyalya.sto.dto.request.RequestCreateDTO;
import kz.lyalya.sto.dto.request.RequestDTO;
import kz.lyalya.sto.dto.review.ReviewCreateDTO;
import kz.lyalya.sto.dto.review.ReviewDTO;
import kz.lyalya.sto.dto.review.ReviewUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReviewServiceTest {
    private final long REQUEST_ID = 7;
    private final long ID = 1;
    private final String CLIENT_EMAIL = "client7@mail.ru";
    private final String MASTER_EMAIL = "master7@mail.ru";

    @Autowired
    private ReviewServiceImpl reviewService;

    @Test
    @Order(1)
    @Sql(value = {"/sql/users_roles.sql", "/sql/reviews_test.sql"})
    public void shouldAdd() {
        short rate = 9;

        ReviewCreateDTO dto = new ReviewCreateDTO();
        dto.setRequestId(REQUEST_ID);
        dto.setComment("comment");
        dto.setRate(rate);

        ReviewDTO result = reviewService.add(dto);

        Assertions.assertNotNull(result);
    }

    @Test
    @Order(2)
    public void shouldUpdate() {
        short rate = 10;

        ReviewUpdateDTO dto = new ReviewUpdateDTO();
        dto.setId(ID);
        dto.setComment("updated");
        dto.setRate(rate);

        ReviewDTO result = reviewService.update(dto);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getRate(), rate);
        Assertions.assertEquals(result.getComment(), "updated");
    }

    @Test
    @Order(3)
    public void shouldDelete() {
        reviewService.delete(ID);

        Assertions.assertThrows(EntityNotFoundException.class, () -> reviewService.findOne(ID));
    }

}
