package kz.lyalya.sto.service.impl;

import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.common_service.impl.request.RequestServiceImpl;
import kz.lyalya.sto.dto.request.RequestCreateDTO;
import kz.lyalya.sto.dto.request.RequestDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.exception.OtherClientException;
import kz.lyalya.sto.exception.RequestInWorkException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RequestServiceTest {
    private final long CAR_ID = 5;
    private final long MASTER_ID = 5;
    private final long ID = 1;
    private final String CLIENT_EMAIL = "client5@mail.ru";
    private final String MASTER_EMAIL = "master5@mail.ru";

    @Autowired
    private RequestServiceImpl requestService;

    @Test
    @Order(1)
    @Sql(value = {"/sql/users_roles.sql", "/sql/requests_test.sql"})
    public void shouldAdd() {
        RequestCreateDTO dto = new RequestCreateDTO();
        dto.setCarId(CAR_ID);
        dto.setMasterId(MASTER_ID);
        dto.setProblemDescription("Lalala");

        RequestDTO result = requestService.add(dto, CLIENT_EMAIL);
        Assertions.assertNotNull(result);
    }

    @Test
    @Order(2)
    public void shouldUpdate() {
        RequestCreateDTO dto = new RequestCreateDTO();
        dto.setId(ID);
        dto.setCarId(CAR_ID);
        dto.setMasterId(MASTER_ID);
        dto.setProblemDescription("Updated");

        RequestDTO result = requestService.update(dto, CLIENT_EMAIL);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getProblemDescription(), "Updated");
    }

    @Test
    @Order(3)
    public void shouldChangeState() {
        RequestState state = RequestState.IN_PROCESS;
        Request request = requestService.changeState(ID, state);

        Assertions.assertNotNull(request);
        Assertions.assertEquals(request.getState(), state);

        requestService.changeState(ID, RequestState.CREATED);
    }

    @Test
    @Order(4)
    public void shouldBeginWork() {
        RequestDTO request = requestService.beginWork(MASTER_EMAIL, ID);

        Assertions.assertNotNull(request);
        Assertions.assertEquals(RequestState.valueOf(request.getState()), RequestState.IN_PROCESS);

        requestService.changeState(ID, RequestState.CREATED);
    }

    @Test
    @Order(5)
    public void shouldFinishWork() {
        Assertions.assertThrows(RequestInWorkException.class, () -> requestService.finishWork(MASTER_EMAIL, ID));
    }

    @Test
    @Order(6)
    public void shouldDelete() {
        Assertions.assertThrows(OtherClientException.class, () -> requestService.delete(ID, MASTER_EMAIL));

        requestService.changeState(ID, RequestState.CREATED);

        requestService.delete(ID, CLIENT_EMAIL);
        Assertions.assertThrows(EntityNotFoundException.class, () -> requestService.findOne(ID));
    }
}
