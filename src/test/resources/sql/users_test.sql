DELETE FROM clients WHERE ID BETWEEN 1 AND 12;
DELETE FROM users_roles WHERE user_id BETWEEN 1 AND 12;
DELETE FROM users_photos WHERE user_id BETWEEN 1 AND 12;
DELETE FROM users WHERE ID BETWEEN 1 AND 12;

INSERT INTO users(id, name, surname, date_of_birth, phone_number, email, password, updated, date_of_creation, is_enabled, deleted)
VALUES
    (1, 'test1', 'test1', '2024-04-05', '+77777777777', 'test1@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false),
    (2, 'test2', 'test2', '2024-04-05', '+77777777777', 'admin@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false);
