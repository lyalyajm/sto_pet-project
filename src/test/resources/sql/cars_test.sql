INSERT INTO users(id, name, surname, date_of_birth, phone_number, email, password, updated, date_of_creation, is_enabled, deleted)
VALUES
    (6, 'client', 'client', '2024-04-05', '+77777777777', 'client@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false);

INSERT INTO clients(user_id)
VALUES (6);
