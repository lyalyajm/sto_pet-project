INSERT INTO users(id, name, surname, date_of_birth, phone_number, email, password, updated, date_of_creation, is_enabled, deleted)
VALUES
    (8, 'client', 'client', '2024-04-05', '+77777777777', 'client5@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false),
    (9, 'master', 'master', '2024-04-05', '+77777777777', 'master5@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false);

INSERT INTO clients(id, user_id)
VALUES (5, 8);

INSERT INTO masters(id, user_id, experience, avg_rate, description, is_workable)
VALUES (5, 9, 5, 0, 'master', true);

INSERT INTO cars(id, mark, model, year_of_produce, state, deleted, date_of_creation, client_id)
VALUES (5, 37, 'Civic', 2018, 1, false, '2024-04-10', 5);
