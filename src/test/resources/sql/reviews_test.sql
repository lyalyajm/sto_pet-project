INSERT INTO users(id, name, surname, date_of_birth, phone_number, email, password, updated, date_of_creation, is_enabled, deleted)
VALUES
    (12, 'client', 'client', '2024-04-05', '+77777777777', 'client12@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false),
    (13, 'master', 'master', '2024-04-05', '+77777777777', 'master13@mail.ru', '$2a$10$TN6q4VZiHvme9MnwzYxaJOKN1TDwe4WRB6UUuIBxxO1UUy6eF1Xru', '2024-04-04', '2024-04-04', true, false);

INSERT INTO clients(id, user_id)
VALUES (7, 12);

INSERT INTO masters(id, user_id, experience, avg_rate, description, is_workable)
VALUES (7, 13, 5, 0, 'master', true);

INSERT INTO cars(id, mark, model, year_of_produce, state, deleted, date_of_creation, client_id)
VALUES (7, 37, 'Civic', 2018, 1, false, '2024-04-10', 7);

INSERT INTO requests(id, client_id, master_id, car_id, problem_description, date_of_creation, date_of_start, date_of_finish, deleted, state, final_price, current_price)
VALUES (7, 7, 7, 7, 'test', '2024-04-10', '2024-04-10', '2024-04-10', false, 2, 50000, 50000);

INSERT INTO done_works(id, request_id, work_type, note, price, date)
VALUES (7, 7, 2, 'note', 50000, '2024-04-10');
