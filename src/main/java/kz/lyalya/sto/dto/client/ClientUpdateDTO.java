package kz.lyalya.sto.dto.client;

import kz.lyalya.sto.util.validator.EmailValidatable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClientUpdateDTO implements EmailValidatable {
    @NotNull(message = "id не должно быть null")
    private Long id;

    @NotNull(message = "Укажите имя")
    @Size(min = 2, max = 30, message = "Диапазон имени 3-30 символов")
    private String name;

    @NotNull(message = "Укажите фамилию")
    @Size(min = 1, max = 30, message = "Диапазон фамилии 1-30 символов")
    private String surname;

    @NotNull(message = "Укажите дату рождения")
    private LocalDate dateOfBirth;

    @NotNull(message = "Укажите сотовый номер")
    @Pattern(regexp = "^((8|\\+374|\\+994|\\+995|\\+375|\\+7|\\+380|\\+38|\\+996|\\+998|\\+993)[\\- ]?)?\\(?\\d{3,5}\\)?[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}(([\\- ]?\\d{1})?[\\- ]?\\d{1})?$",
            message = "Введите корректный номер телефона")
    private String phoneNumber;

    @Pattern(regexp = "^((8|\\+374|\\+994|\\+995|\\+375|\\+7|\\+380|\\+38|\\+996|\\+998|\\+993)[\\- ]?)?\\(?\\d{3,5}\\)?[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}(([\\- ]?\\d{1})?[\\- ]?\\d{1})?$",
            message = "Введите корректный номер телефона")
    private String phoneNumber2;

    @NotNull(message = "Укажите электронную почту")
    @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$",
            message = "Введите корректный email")
    private String email;

    @Override
    public String getUserEmail() {
        return this.email;
    }
}
