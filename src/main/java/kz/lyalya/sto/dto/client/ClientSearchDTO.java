package kz.lyalya.sto.dto.client;

import kz.lyalya.sto.common_data.pagination_specification.common.FilterModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientSearchDTO implements FilterModel {
    private String name;
    private String surname;
}
