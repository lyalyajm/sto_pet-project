package kz.lyalya.sto.dto.client;

import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.UserPhoto;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ClientDTO {
    private long id;
    private String name;
    private String surname;
    private LocalDate dateOfBirth;
    private String phoneNumber;
    private String phoneNumber2;
    private String email;
    private boolean isEnabled = true;
    private LocalDateTime dateOfCreation;
    private boolean isDeleted;
    private List<UserPhoto> userPhoto;
    private List<Long> carsIds;
    private List<Long> requestIds;

    public static ClientDTO convertToDto(Client client) {
        return ClientDTO.builder()
                .id(client.getId())
                .name(client.getUser().getName())
                .surname(client.getUser().getSurname())
                .dateOfBirth(client.getUser().getDateOfBirth())
                .email(client.getUser().getEmail())
                .isDeleted(client.getUser().isDeleted())
                .phoneNumber(client.getUser().getPhoneNumber())
                .phoneNumber2(client.getUser().getPhoneNumber2() == null ? "" : client.getUser().getPhoneNumber2())
                .userPhoto(client.getUser().getUserPhotos() != null || !client.getUser().getUserPhotos().isEmpty() ?
                            client.getUser().getUserPhotos() : Collections.emptyList())
                .carsIds(client.getCars() != null ?
                        client.getCars().stream().map(Car::getId).collect(Collectors.toList()) : Collections.emptyList())
                .requestIds(client.getRequests() != null ?
                        client.getRequests().stream().map(Request::getId).collect(Collectors.toList()) : Collections.emptyList())
                .build();
    }
}
