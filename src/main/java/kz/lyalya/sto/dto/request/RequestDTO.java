package kz.lyalya.sto.dto.request;

import kz.lyalya.sto.dto.review.ReviewDTO;
import kz.lyalya.sto.common_data.entity.request.Request;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static java.util.Objects.isNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RequestDTO {
    private long id;
    private Long clientId;
    private String clientName;
    private Long masterId;
    private String masterName;
    private Long carId;
    private String carName;
    private String problemDescription;
    private LocalDateTime dateOfCreation;
    private LocalDate dateOfStart;
    private LocalDate dateOfFinish;
    private boolean isDeleted;
    private String state;
    private int price;
    private int currentPrice;
    private ReviewDTO review; //maybe to send review entity

    public static RequestDTO convertToDto(Request request) {
        return RequestDTO.builder()
                .id(request.getId())
                .clientId(request.getClient().getId())
                .clientName(request.getClient().getUser().getName() + " " + request.getClient().getUser().getSurname())
                .masterId(request.getMaster().getId())
                .masterName(request.getMaster().getUser().getName() + " " + request.getMaster().getUser().getSurname())
                .carId(request.getCar().getId())
                .carName(request.getCar().getMark() + " " + request.getCar().getModel())
                .problemDescription(request.getProblemDescription())
                .dateOfCreation(request.getDateOfCreation())
                .dateOfStart(isNull(request.getDateOfStart()) ? null : request.getDateOfStart())
                .dateOfFinish(isNull(request.getDateOfFinish()) ? null : request.getDateOfFinish())
                .isDeleted(request.isDeleted())
                .state(request.getState().toString())
                .price(isNull(request.getPrice()) ? 0 : request.getPrice())
                .currentPrice(request.getCurrentPrice())
                .review(!isNull(request.getReview()) ? ReviewDTO.convertToDto(request.getReview()) : null)
                .build();

    }
}
