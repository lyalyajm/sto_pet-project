package kz.lyalya.sto.dto.request;

import kz.lyalya.sto.common_data.pagination_specification.common.FilterModel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RequestSearchDTO implements FilterModel {
    private long masterId;
    private String masterName;
    private long clientId;
    private String clientName;
    private LocalDate from;
    private LocalDate to;
    private int priceFrom;
    private int priceTo;
    private short rateFrom;
    private short rateTo;
    private String carMark;
    private Boolean finished;
}
