package kz.lyalya.sto.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RequestCreateDTO {
    private long id = 0;

    @NotNull(message = "Выберите мастера")
    private long masterId;

    @NotNull(message = "Выберите машину")
    private long carId;

    @NotNull(message = "Опишите проблему, можно буквально в двух словах")
    @Size(min = 3, max = 500, message = "Описание проблемы в рамках от 3 до 500 символов")
    private String problemDescription;
}
