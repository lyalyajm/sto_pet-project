package kz.lyalya.sto.dto.car;

import jakarta.persistence.Enumerated;
import kz.lyalya.sto.common_data.enumeration.CarMark;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CarUpdateDTO {
    @NotNull
    private long id;

    @Enumerated
    @NotNull(message = "Выберите марку машины")
    private CarMark mark;

    @NotNull(message = "Укажите модель машины")
    private String model;

    @NotNull(message = "Укажите год производства")
    @Min(value = 1950, message = "Указан некорректный год производства")
    private int yearOfProduce;
}
