package kz.lyalya.sto.dto.car;

import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.car.CarPhoto;
import kz.lyalya.sto.common_data.entity.request.Request;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CarDTO {
    private long id;
    private String mark;
    private String model;
    private int yearOfProduce;
    private String state;
    private LocalDateTime dateOfCreation;
    private boolean isDeleted;
    private Long ownerId;
    private List<CarPhoto> photosIds;
    private List<Long> requestsIds;

    public static CarDTO convertToDto(Car car) {
        return CarDTO.builder()
                .id(car.getId())
                .mark(car.getMark().toString())
                .model(car.getModel())
                .yearOfProduce(car.getYearOfProduce())
                .state(car.getState().toString())
                .isDeleted(car.isDeleted())
                .ownerId(car.getOwner().getId())
                .photosIds(car.getPhotos())
                .requestsIds(car.getRequests().stream().map(Request::getId).collect(Collectors.toList()))
                .build();
    }
}
