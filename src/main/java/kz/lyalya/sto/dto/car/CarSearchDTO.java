package kz.lyalya.sto.dto.car;

import kz.lyalya.sto.common_data.pagination_specification.common.FilterModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarSearchDTO implements FilterModel {
    private String mark;
    private String model;
    private long clientId;
    private int yearOfProduce;
}
