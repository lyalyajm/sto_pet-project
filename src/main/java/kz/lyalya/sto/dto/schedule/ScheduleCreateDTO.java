package kz.lyalya.sto.dto.schedule;

import kz.lyalya.sto.util.validator.TimeValidatable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScheduleCreateDTO implements TimeValidatable {
    @NotNull(message = "Не найдена заявка для создания записи")
    private Long requestId;

    @NotNull(message = "Необходимо указать дату и время начала работы")
    private LocalDateTime dateTimeOsStart;

    @NotNull(message = "Необходимо указать дату и время окончания работы")
    private LocalDateTime dateTimeOsFinish;

    @Override
    public LocalDateTime getDateTimeOfStart() {
        return this.dateTimeOsStart;
    }

    @Override
    public LocalDateTime getDateTimeOfFinish() {
        return this.dateTimeOsFinish;
    }
}
