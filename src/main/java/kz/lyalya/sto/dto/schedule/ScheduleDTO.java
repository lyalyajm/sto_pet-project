package kz.lyalya.sto.dto.schedule;

import kz.lyalya.sto.dto.request.RequestDTO;
import kz.lyalya.sto.common_data.entity.schedule.Schedule;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ScheduleDTO {
    private long id;
    private RequestDTO requestDTO;
    private LocalDateTime dateTimeOsStart;
    private LocalDateTime dateTimeOsFinish;

    public static ScheduleDTO convertToDto(Schedule schedule) {
        return ScheduleDTO.builder()
                .id(schedule.getId())
                .requestDTO(RequestDTO.convertToDto(schedule.getRequest()))
                .dateTimeOsStart(schedule.getDateTimeOsStart())
                .dateTimeOsFinish(schedule.getDateTimeOsFinish())
                .build();
    }
}
