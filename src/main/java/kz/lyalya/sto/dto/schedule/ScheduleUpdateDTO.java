package kz.lyalya.sto.dto.schedule;

import jakarta.validation.constraints.NotNull;
import kz.lyalya.sto.util.validator.TimeValidatable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleUpdateDTO implements TimeValidatable {
    @NotNull(message = "Введите корректный id для изменения")
    private long id;

    @NotNull(message = "Необходимо указать дату и время начала работы")
    private LocalDateTime dateTimeOsStart;

    @NotNull(message = "Необходимо указать дату и время окончания работы")
    private LocalDateTime dateTimeOsFinish;

    @Override
    public LocalDateTime getDateTimeOfStart() {
        return this.dateTimeOsStart;
    }

    @Override
    public LocalDateTime getDateTimeOfFinish() {
        return this.dateTimeOsFinish;
    }
}
