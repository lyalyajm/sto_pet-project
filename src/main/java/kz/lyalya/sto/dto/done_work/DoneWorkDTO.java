package kz.lyalya.sto.dto.done_work;

import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_data.enumeration.WorkType;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DoneWorkDTO {
    private long id;
    private long requestId;
    private WorkType workType;
    private String note;
    private int price;
    private LocalDate date;

    public static DoneWorkDTO convertToDto(DoneWork doneWork) {
        return DoneWorkDTO.builder()
                .id(doneWork.getId())
                .requestId(doneWork.getRequest().getId())
                .workType(doneWork.getWorkType())
                .note(doneWork.getNote() != null ? doneWork.getNote() : "")
                .price(doneWork.getPrice())
                .date(doneWork.getDate())
                .build();
    }
}
