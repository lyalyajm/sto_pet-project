package kz.lyalya.sto.dto.done_work;

import kz.lyalya.sto.common_data.pagination_specification.common.FilterModel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DoneWorkSearchDTO implements FilterModel {
    private long masterId;
    private long carId;
    private long clientId;
    private LocalDate from;
    private LocalDate to;
    private String workType;
}
