package kz.lyalya.sto.dto.done_work;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DoneWorkUpdateDTO {
    private long id = 0;

    @NotNull(message = "Укажите тип работы")
    private String workType;

    private String note;

    @NotNull(message = "Укажите стоимость работы")
    private int price;

    private LocalDate date;
}
