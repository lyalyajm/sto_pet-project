package kz.lyalya.sto.dto.review;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReviewUpdateDTO {
    private long id;
    private String comment;

    @NotNull(message = "Оцените работу матстера СТО")
    @Min(value = 1, message = "Минимальная оценка: 1")
    @Max(value = 10, message = "Максимальная оценка: 10")
    private short rate;
}