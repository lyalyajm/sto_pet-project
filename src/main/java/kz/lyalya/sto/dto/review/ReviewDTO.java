package kz.lyalya.sto.dto.review;

import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.entity.review.ReviewPhoto;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ReviewDTO {
    private long id;
    private Long requestId;
    private String comment;
    private short rate;
    private LocalDate date;
    private List<ReviewPhoto> photos;

    public static ReviewDTO convertToDto(Review review) {
        return ReviewDTO.builder()
                .id(review.getId())
                .requestId(review.getRequest().getId())
                .comment(review.getComment())
                .rate(review.getRate())
                .date(review.getDate())
                .photos(review.getPhotos())
                .build();
    }
}
