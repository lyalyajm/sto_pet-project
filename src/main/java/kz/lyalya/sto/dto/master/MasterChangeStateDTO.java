package kz.lyalya.sto.dto.master;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MasterChangeStateDTO {
    private long id;
    private boolean isWorkable;
}
