package kz.lyalya.sto.dto.master;

import kz.lyalya.sto.common_data.pagination_specification.common.FilterModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MasterSearchDTO implements FilterModel {
    private String name;
    private String surname;
    private short rate;
}