package kz.lyalya.sto.dto.master;

import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.entity.user.UserPhoto;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MasterDTO {
    private long id;
    private String name;
    private String surname;
    private LocalDate dateOfBirth;
    private String phoneNumber;
    private String phoneNumber2;
    private String email;
    private boolean isEnabled = true;
    private LocalDateTime dateOfCreation;
    private boolean isDeleted;
    private List<UserPhoto> userPhoto;
    private List<Long> requestIds;
    private short experience;
    private short avgRate;
    private String description;

    public static MasterDTO convertToDto(Master master) {
        return MasterDTO.builder()
                .id(master.getId())
                .name(master.getUser().getName())
                .surname(master.getUser().getSurname())
                .dateOfBirth(master.getUser().getDateOfBirth())
                .email(master.getUser().getEmail())
                .isDeleted(master.getUser().isDeleted())
                .phoneNumber(master.getUser().getPhoneNumber())
                .phoneNumber2(master.getUser().getPhoneNumber2() == null ? "" : master.getUser().getPhoneNumber2())
                .experience(master.getExperience())
                .avgRate(master.getAvgRate())
                .description(master.getDescription())
                .userPhoto(master.getUser().getUserPhotos())
                .requestIds(master.getRequests().stream().map(Request::getId).collect(Collectors.toList()))
                .build();
    }
}
