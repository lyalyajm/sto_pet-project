package kz.lyalya.sto.common_web.controller.car;

import kz.lyalya.sto.common_service.impl.car.CarBelongsToClientChecker;
import kz.lyalya.sto.dto.car.CarCreateDTO;
import kz.lyalya.sto.dto.car.CarDTO;
import kz.lyalya.sto.dto.car.CarSearchDTO;
import kz.lyalya.sto.dto.car.CarUpdateDTO;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_service.impl.car.CarServiceImpl;
import kz.lyalya.sto.exception.CarDoesNotBelongsToClientException;
import kz.lyalya.sto.exception.IncorrectFieldsException;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/cars")
public class CarController {
    private final CarServiceImpl carService;
    private final FileUploadService fileUploadService;
    private final CarBelongsToClientChecker carChecker;

    @Value("${files_cars_upload}")
    private String root;

    @GetMapping("/id/{id}")
    private ResponseEntity<CarDTO> getOne(@PathVariable("id") long id) {
        return ResponseEntity.ok(carService.getOne(id));
    }

    @PostMapping("/add")
    private ResponseEntity<CarDTO> add(@Valid CarCreateDTO dto, Principal principal,
                                       @RequestParam(value = "file", required = false) MultipartFile file,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        if (file != null) {
            return ResponseEntity.ok(carService.add(dto, file, principal.getName()));
        }
        return ResponseEntity.ok(carService.add(dto, principal.getName()));
    }

    @PostMapping("/{id}/update")
    private ResponseEntity<CarDTO> update(@PathVariable("id") long id, @RequestBody @Valid CarUpdateDTO dto,
                                          Principal principal, BindingResult bindingResult) {
        if (!carChecker.checkEntityBelongsToUser(carService.findOne(id), principal.getName())) {
            throw new CarDoesNotBelongsToClientException(dto.getMark() + dto.getModel(), principal.getName());
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(carService.update(dto));
    }

    @PostMapping("/{id}/update-photo")
    private ResponseEntity<CarDTO> updatePhoto(@PathVariable long id, @RequestParam("file") MultipartFile file,
                                               @RequestParam boolean isMainPhoto, Principal principal) {
        if (!carChecker.checkEntityBelongsToUser(carService.findOne(id), principal.getName())) {
            throw new CarDoesNotBelongsToClientException(String.valueOf(id), principal.getName());
        }
        return ResponseEntity.ok(carService.updatePhoto(id, file, isMainPhoto));
    }

    @PostMapping("/{id}/delete-photo")
    private ResponseEntity<CarDTO> deletePhoto(@PathVariable long id, @RequestParam String filePath,
                                               Principal principal) {
        if (!carChecker.checkEntityBelongsToUser(carService.findOne(id), principal.getName())) {
            throw new CarDoesNotBelongsToClientException(String.valueOf(id), principal.getName());
        }
        carService.deletePhoto(id, filePath);
        return ResponseEntity.ok(carService.getOne(id));
    }

    @PostMapping("/{id}/delete")
    private ResponseEntity delete(@PathVariable("id") long id, Principal principal) {
        if (!carChecker.checkEntityBelongsToUser(carService.findOne(id), principal.getName())) {
            throw new CarDoesNotBelongsToClientException(String.valueOf(id), principal.getName());
        }
        carService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<Page<CarDTO>> all(@RequestBody SearchModel<CarSearchDTO> searchModel) {
        return ResponseEntity.ok(carService.search(searchModel));
    }

    @GetMapping("/photo/{filename:.+}")
    public ResponseEntity<Resource> getFilePic(@PathVariable String filename) {
        {
            Resource file = fileUploadService.loadAsResource(filename, root);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
    }
}
