package kz.lyalya.sto.common_web.controller.client;

import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientDTO;
import kz.lyalya.sto.dto.client.ClientSearchDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_service.impl.user.ClientServiceImpl;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import kz.lyalya.sto.exception.IncorrectFieldsException;
import kz.lyalya.sto.exception.OtherClientException;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import kz.lyalya.sto.util.validator.UserValidator;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/clients")
public class ClientController {
    private final ClientServiceImpl clientService;
    private final FileUploadService fileUploadService;
    private final UserValidator userValidator;

    @Value("${files_users_upload}")
    private String root;

    @Value("${files_upload}")
    private String mainRoot;

    @GetMapping("/id/{id}")
    private ResponseEntity<ClientDTO> getOne(@PathVariable("id") long id) {
        return ResponseEntity.ok(clientService.getOne(id));
    }

    @PostMapping("/registration")
    private ResponseEntity<ClientDTO> add(@Valid ClientCreateDTO dto,  BindingResult bindingResult,
                                          @RequestParam(value = "file", required = false) MultipartFile file) {
        userValidator.validate(dto, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        if (file != null) {
            return ResponseEntity.ok(clientService.add(dto, file));
        }
        return ResponseEntity.ok(clientService.add(dto));
    }

    @PostMapping("/{id}/update")
    private ResponseEntity<ClientDTO> update(@PathVariable("id") long id, Principal principal,
                                             @RequestBody @Valid ClientUpdateDTO dto,
                                             BindingResult bindingResult) {
        if (!clientService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherClientException("Обновлять данные может только сам пользователь");
        }

        dto.setId(id);
        if (!clientService.getOne(id).getEmail().equals(dto.getEmail())) {
            userValidator.validate(dto, bindingResult);
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(clientService.update(dto, principal.getName()));
    }

    @PostMapping("/{id}/update-password")
    private ResponseEntity<ClientDTO> updatePassword(@PathVariable("id") long id, Principal principal,
                                                     @RequestBody @Valid UserPasswordUpdateDTO dto,
                                                     BindingResult bindingResult) {
        if (!clientService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherClientException("Обновлять данные может только сам пользователь");
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(clientService.updatePassword(dto, id, principal.getName()));
    }

    @PostMapping("/{id}/update-photo")
    private ResponseEntity<ClientDTO> updatePhoto(@PathVariable long id, Principal principal,
                                                  @RequestParam("file") MultipartFile file,
                                                  @RequestParam boolean isMainPhoto) {

        if (!clientService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherClientException("Обновлять данные может только сам пользователь");
        }
        return ResponseEntity.ok(clientService.updatePhoto(id, file, isMainPhoto, principal.getName()));
    }

    @PostMapping("/{id}/delete-photo")
    private ResponseEntity<ClientDTO> deletePhoto(@PathVariable long id, Principal principal,
                                                  @RequestParam String filePath) {
        if (!clientService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherClientException("Обновлять данные может только сам пользователь");
        }
        return ResponseEntity.ok(clientService.deletePhoto(id, filePath, principal.getName()));
    }

    @PostMapping("/{id}/delete")
    private ResponseEntity delete(@PathVariable("id") long id, Principal principal) {
        if (!clientService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherClientException("Удаление может производить только сам пользователь");
        }
        clientService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<Page<ClientDTO>> all(@RequestBody SearchModel<ClientSearchDTO> searchModel) {
        return ResponseEntity.ok(clientService.search(searchModel));
    }

    @GetMapping("/photo/{filename:.+}")
    public ResponseEntity<Resource> getFilePic(@PathVariable String filename) {
        {
            Resource file = fileUploadService.loadAsResource(filename, root);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
    }

    @GetMapping(value = "/test/{filename:.+}")
    public @ResponseBody byte[] getImage(@PathVariable String filename) throws IOException {
        Resource file = fileUploadService.loadAsResource(filename, root);
        InputStream in = file.getInputStream();
        return IOUtils.toByteArray(in);
    }
}

