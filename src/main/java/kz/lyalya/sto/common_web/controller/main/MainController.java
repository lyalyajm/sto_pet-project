package kz.lyalya.sto.common_web.controller.main;

import kz.lyalya.sto.common_data.enumeration.WorkType;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class MainController {

    @GetMapping("/works")
    public ResponseEntity<List<WorkType>> getWorks(Principal principal) {
        return ResponseEntity.ok(List.of(WorkType.values()));
    }
}
