package kz.lyalya.sto.common_web.controller.review;

import kz.lyalya.sto.common_service.impl.car.CarBelongsToClientChecker;
import kz.lyalya.sto.common_service.impl.request.RequestBelongsToClientChecker;
import kz.lyalya.sto.common_service.impl.request.RequestServiceImpl;
import kz.lyalya.sto.common_service.impl.review.ReviewServiceImpl;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.EntityBelongsChecker;
import kz.lyalya.sto.dto.review.ReviewCreateDTO;
import kz.lyalya.sto.dto.review.ReviewDTO;
import kz.lyalya.sto.dto.review.ReviewSearchDTO;
import kz.lyalya.sto.dto.review.ReviewUpdateDTO;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.exception.CarDoesNotBelongsToClientException;
import kz.lyalya.sto.exception.IncorrectFieldsException;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/reviews")
public class ReviewController {
    private final ReviewServiceImpl reviewService;
    private final RequestServiceImpl requestService;
    private final FileUploadService fileUploadService;
    private EntityBelongsChecker entityBelongsChecker;

    @Value("${files_reviews_upload}")
    private String root;

    @GetMapping("/id/{id}")
    private ResponseEntity<ReviewDTO> getOne(@PathVariable("id") long id) {
        return ResponseEntity.ok(reviewService.getOne(id));
    }

    @PostMapping("/add")
    private ResponseEntity<ReviewDTO> add(@Valid ReviewCreateDTO dto,
                                          @RequestParam(value = "file", required = false) MultipartFile file,
                                          @RequestParam(value = "file2", required = false) MultipartFile file2,
                                          @RequestParam(value = "file3", required = false) MultipartFile file3,
                                          Principal principal, BindingResult bindingResult) {
        entityBelongsChecker = new RequestBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(requestService.findOne(dto.getRequestId()), principal.getName())) {
            throw new CarDoesNotBelongsToClientException("Доступ к добавлению отзыва доступен только владельцу авто");
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }

        if (file == null && file2 == null && file3 == null) {
            return ResponseEntity.ok(reviewService.add(dto));
        } else if(file2 == null && file3 == null) {
            reviewService.checkFilenames(file.getOriginalFilename());
            return ResponseEntity.ok(reviewService.add(dto, file));
        } else if(file3 == null) {
            reviewService.checkFilenames(file.getOriginalFilename(), file2.getOriginalFilename());
            return ResponseEntity.ok(reviewService.add(dto, file, file2));
        }
        reviewService.checkFilenames(file.getOriginalFilename(), file2.getOriginalFilename(), file3.getOriginalFilename());
        return ResponseEntity.ok(reviewService.add(dto, file, file2, file3));
    }

    @PostMapping("/{id}/update")
    private ResponseEntity<ReviewDTO> update(@PathVariable("id") long id,
                                             @RequestBody @Valid ReviewUpdateDTO dto,
                                             BindingResult bindingResult, Principal principal) {
        entityBelongsChecker = new CarBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(reviewService.findOne(id).getRequest().getCar(), principal.getName())) {
            throw new CarDoesNotBelongsToClientException("Доступ к добавлению отзыва доступен только владельцу авто");
        }
        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        dto.setId(id);
        return ResponseEntity.ok(reviewService.update(dto));
    }

    @PostMapping("/{id}/update-photo")
    private ResponseEntity<ReviewDTO> updatePhoto(@PathVariable long id,
                                                  @RequestParam("file") MultipartFile file,
                                                  Principal principal) {
        entityBelongsChecker = new CarBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(reviewService.findOne(id).getRequest().getCar(), principal.getName())) {
            throw new CarDoesNotBelongsToClientException("Доступ к добавлению отзыва доступен только владельцу авто");
        }
        return ResponseEntity.ok(reviewService.updatePhoto(id, file));
    }

    @PostMapping("/{id}/delete-photo")
    private ResponseEntity<ReviewDTO> deletePhoto(@PathVariable long id,
                                                  @RequestParam String filePath,
                                                  Principal principal) {
        entityBelongsChecker = new CarBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(reviewService.findOne(id).getRequest().getCar(), principal.getName())) {
           throw new CarDoesNotBelongsToClientException("Доступ к добавлению отзыва доступен только владельцу авто");
        }
        return ResponseEntity.ok(reviewService.deletePhoto(id, filePath));
    }

    @PostMapping("/{id}/delete")
    private ResponseEntity delete(@PathVariable("id") long id, Principal principal) {
        entityBelongsChecker = new CarBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(reviewService.findOne(id).getRequest().getCar(), principal.getName())) {
            throw new CarDoesNotBelongsToClientException("Доступ к добавлению отзыва доступен только владельцу авто");
        }
        reviewService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<Page<ReviewDTO>> all(@RequestBody SearchModel<ReviewSearchDTO> searchModel) {
        return ResponseEntity.ok(reviewService.search(searchModel));
    }

    @GetMapping("/photo/{filename:.+}")
    public ResponseEntity<Resource> getFilePic(@PathVariable String filename) {
        {
            Resource file = fileUploadService.loadAsResource(filename, root);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
    }
}
