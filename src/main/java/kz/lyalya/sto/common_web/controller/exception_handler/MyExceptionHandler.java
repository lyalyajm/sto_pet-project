package kz.lyalya.sto.common_web.controller.exception_handler;

import kz.lyalya.sto.dto.ErrorModel;
import kz.lyalya.sto.exception.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;

@ControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<ErrorModel> handleEntityNotFoundException(EntityNotFoundException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({EntityDeleteException.class})
    public ResponseEntity<ErrorModel> handleEntityDeleteException(EntityDeleteException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({IncorrectFileFormatException.class})
    public ResponseEntity<ErrorModel> handleIncorrectFileFormatException(IncorrectFileFormatException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({StorageException.class})
    public ResponseEntity<ErrorModel> handleStorageException(StorageException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({SQLException.class})
    public ResponseEntity<ErrorModel> handleSQLException(SQLException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({CarDoesNotBelongsToClientException.class})
    public ResponseEntity<ErrorModel> handleCarDoesNotBelongsToClientException(CarDoesNotBelongsToClientException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({IncorrectPasswordException.class})
    public ResponseEntity<ErrorModel> handleIncorrectPasswordException(IncorrectPasswordException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({IncorrectFieldsException.class})
    public ResponseEntity<ErrorModel> handleIncorrectFieldsException(IncorrectFieldsException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<ErrorModel> handleIllegalArgumentException(IllegalArgumentException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({OtherMastersException.class})
    public ResponseEntity<ErrorModel> handleOtherMastersException(OtherMastersException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({OtherClientException.class})
    public ResponseEntity<ErrorModel> handleOtherClientException(OtherClientException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({RequestInWorkException.class})
    public ResponseEntity<ErrorModel> handleRequestInWorkException(RequestInWorkException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

    @ExceptionHandler({RequestWasFinishedException.class})
    public ResponseEntity<ErrorModel> handleRequestWasFinishedException(RequestWasFinishedException exception) {
        ErrorModel error = ErrorModel.builder()
                .message(exception.getMessage())
                .build();
        return ResponseEntity.ok(error);
    }

//    @ExceptionHandler({MethodArgumentNotValidException.class})
//    public ResponseEntity<ErrorModel> handleMethodArgumentNotValidException(MethodArgumentNotValidException  exception) {
//        ErrorModel error = ErrorModel.builder()
//                .message(exception.getMessage())
//                .build();
//        return ResponseEntity.ok(error);
//    }
}
