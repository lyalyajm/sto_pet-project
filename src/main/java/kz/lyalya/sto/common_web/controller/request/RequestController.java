package kz.lyalya.sto.common_web.controller.request;

import kz.lyalya.sto.dto.request.RequestCreateDTO;
import kz.lyalya.sto.dto.request.RequestDTO;
import kz.lyalya.sto.dto.request.RequestSearchDTO;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_service.impl.request.RequestServiceImpl;
import kz.lyalya.sto.exception.IncorrectFieldsException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/requests")
public class RequestController {
    private final RequestServiceImpl requestService;

    @GetMapping("/id/{id}")
    private ResponseEntity<RequestDTO> getOne(@PathVariable("id") long id) {
        return ResponseEntity.ok(requestService.getOne(id));
    }

    @PostMapping("/add")
    private ResponseEntity<RequestDTO> add(@RequestBody @Valid RequestCreateDTO dto,
                                           Principal principal, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(requestService.add(dto, principal.getName()));
    }

    @PostMapping("/{id}/delete")
    private ResponseEntity delete(@PathVariable("id") long id, Principal principal) {
        requestService.delete(id, principal.getName());
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/{id}/update")
    private ResponseEntity update(@PathVariable("id") long id,
                                  @RequestBody @Valid RequestCreateDTO dto,
                                  Principal principal, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        dto.setId(id);
        requestService.update(dto, principal.getName());
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<Page<RequestDTO>> all(@RequestBody SearchModel<RequestSearchDTO> searchModel) {
        return ResponseEntity.ok(requestService.search(searchModel));
    }

    @PostMapping("/{id}/begin")
    public ResponseEntity<RequestDTO> beginWork(@PathVariable("id") long id, Principal principal) {
        return ResponseEntity.ok(requestService.beginWork(principal.getName(), id));
    }

    @PostMapping("/{id}/finish")
    public ResponseEntity<RequestDTO> finishWork(@PathVariable("id") long id, Principal principal) {
        return ResponseEntity.ok(requestService.finishWork(principal.getName(), id));
    }
}
