package kz.lyalya.sto.common_web.controller.done_work;

import kz.lyalya.sto.common_service.impl.done_work.DoneWorkBelongsToMasterChecker;
import kz.lyalya.sto.common_service.impl.done_work.DoneWorkServiceImpl;
import kz.lyalya.sto.common_service.impl.request.RequestBelongsToMasterChecker;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.EntityBelongsChecker;
import kz.lyalya.sto.common_service.intf.request.RequestService;
import kz.lyalya.sto.dto.done_work.DoneWorkCreateDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkSearchDTO;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.dto.done_work.DoneWorkUpdateDTO;
import kz.lyalya.sto.exception.IncorrectFieldsException;
import kz.lyalya.sto.exception.OtherMastersException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/done-works")
public class DoneWorkController {
    private final DoneWorkServiceImpl doneWorkService;
    private EntityBelongsChecker entityBelongsChecker;
    private final RequestService requestService;

    @GetMapping("/id/{id}")
    private ResponseEntity<DoneWorkDTO> getOne(@PathVariable("id") long id) {
        return ResponseEntity.ok(doneWorkService.getOne(id));
    }

    @PostMapping("/add")
    private ResponseEntity<DoneWorkDTO> add(@RequestBody @Valid DoneWorkCreateDTO dto,
                                            Principal principal, BindingResult bindingResult) {
        entityBelongsChecker = new RequestBelongsToMasterChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(requestService.findOne(dto.getRequestId()), principal.getName())) {
            throw new OtherMastersException("Записи о работе может добавлять только мастер, указанный в заявке");
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(doneWorkService.add(dto));
    }

    @PostMapping("/{id}/update")
    private ResponseEntity<DoneWorkDTO> update(@PathVariable("id") long id,
                                               @RequestBody @Valid DoneWorkUpdateDTO dto,
                                               Principal principal, BindingResult bindingResult) {
        entityBelongsChecker = new DoneWorkBelongsToMasterChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(doneWorkService.findOne(dto.getId()), principal.getName())) {
            throw new OtherMastersException("Обновлять запись о работе может только мастер, указанный в заявке");
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        dto.setId(id);
        return ResponseEntity.ok(doneWorkService.update(dto));
    }

    @PostMapping("/{id}/delete")
    private ResponseEntity delete(@PathVariable("id") long id, Principal principal) {
        entityBelongsChecker = new DoneWorkBelongsToMasterChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(doneWorkService.findOne(id), principal.getName())) {
            throw new OtherMastersException("Удалять запись о работе может только мастер, указанный в заявке");
        }
        doneWorkService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<Page<DoneWorkDTO>> all(@RequestBody SearchModel<DoneWorkSearchDTO> searchModel) {
        return ResponseEntity.ok(doneWorkService.search(searchModel));
    }
}
