package kz.lyalya.sto.common_web.controller.master;

import kz.lyalya.sto.dto.master.MasterCreateDTO;
import kz.lyalya.sto.dto.master.MasterDTO;
import kz.lyalya.sto.dto.master.MasterSearchDTO;
import kz.lyalya.sto.dto.master.MasterUpdateDTO;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_service.impl.user.MasterServiceImpl;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import kz.lyalya.sto.exception.IncorrectFieldsException;
import kz.lyalya.sto.exception.OtherMastersException;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import kz.lyalya.sto.util.validator.UserValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;

import java.security.Principal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/masters")
public class MasterController {
    private final MasterServiceImpl masterService;
    private final FileUploadService fileUploadService;
    private final UserValidator userValidator;

    @Value("${files_users_upload}")
    private String root;

    @GetMapping("/id/{id}")
    private ResponseEntity<MasterDTO> getOne(@PathVariable("id") long id) {
        return ResponseEntity.ok(masterService.getOne(id));
    }

    @PostMapping("/add")
    private ResponseEntity<MasterDTO> add(MasterCreateDTO dto, Principal principal,
                                          @RequestParam(value = "file", required = false) MultipartFile file,
                                          BindingResult bindingResult) {
        userValidator.validate(dto, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        if (file != null) {
            return ResponseEntity.ok(masterService.add(dto, file, principal.getName()));
        }
        return ResponseEntity.ok(masterService.add(dto, principal.getName()));
    }

    @PostMapping("/{id}/update")
    private ResponseEntity<MasterDTO> update(@PathVariable("id") long id, Principal principal,
                                             @RequestBody @Valid MasterUpdateDTO dto,
                                             BindingResult bindingResult) {
        if (!masterService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherMastersException("Обновлять данные может только сам мастер");
        }

        if (!masterService.getOne(id).getEmail().equals(dto.getEmail())) {
            userValidator.validate(dto, bindingResult);
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(masterService.update(dto));
    }

    @PostMapping("/{id}/update-password")
    private ResponseEntity<MasterDTO> updatePassword(@PathVariable("id") long id, Principal principal,
                                                     @RequestBody @Valid UserPasswordUpdateDTO dto,
                                                     BindingResult bindingResult) {
        if (!masterService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherMastersException("Обновлять данные может только сам мастер");
        }

        if (bindingResult.hasErrors()) {
            throw new IncorrectFieldsException(bindingResult.getFieldErrors().get(0).getDefaultMessage());
        }
        return ResponseEntity.ok(masterService.updatePassword(dto, id));
    }

    @PostMapping("/{id}/update-photo")
    private ResponseEntity<MasterDTO> updatePhoto(@PathVariable long id,
                                                  @RequestParam("file") MultipartFile file,
                                                  @RequestParam boolean isMainPhoto,
                                                  Principal principal) {
        if (!masterService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherMastersException("Обновлять данные может только сам мастер");
        }
        return ResponseEntity.ok(masterService.updatePhoto(id, file, isMainPhoto));
    }

    @PostMapping("/{id}/delete-photo")
    private ResponseEntity<MasterDTO> deletePhoto(@PathVariable long id, Principal principal,
                                                  @RequestParam String filePath) {
        if (!masterService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherMastersException("Обновлять данные может только сам мастер");
        }
        return ResponseEntity.ok(masterService.deletePhoto(id, filePath));
    }

    @PostMapping("/{id}/delete")
    private ResponseEntity delete(@PathVariable("id") long id, Principal principal) {
        if (!masterService.checkCurrentUser(principal.getName(), id)) {
            throw new OtherMastersException("Обновлять данные может только сам мастер");
        }
        masterService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<Page<MasterDTO>> all(@RequestBody SearchModel<MasterSearchDTO> searchModel) {
        return ResponseEntity.ok(masterService.search(searchModel));
    }

    @GetMapping("/photo/{filename:.+}")
    public ResponseEntity<Resource> getFilePic(@PathVariable String filename) {
        {
            Resource file = fileUploadService.loadAsResource(filename, root);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
    }
}
