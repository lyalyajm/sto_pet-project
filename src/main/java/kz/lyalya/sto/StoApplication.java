package kz.lyalya.sto;

import kz.lyalya.sto.util.file_upload_service.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class StoApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoApplication.class, args);
    }

}
