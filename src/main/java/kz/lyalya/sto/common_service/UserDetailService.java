package kz.lyalya.sto.common_service;

import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.repository.UserRepository;
import kz.lyalya.sto.dto.user.MyUserDetails;
import kz.lyalya.sto.exception.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserDetailService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String username) throws EntityNotFoundException {
        Optional<User> person = userRepository.findByEmail(username); //findByUsername
        if (person.isEmpty()) {
            throw new EntityNotFoundException("Пользователь с email " + username + " не найден");
        }
        return new MyUserDetails(person.get());
    }
}

