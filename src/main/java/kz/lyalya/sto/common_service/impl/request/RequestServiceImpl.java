package kz.lyalya.sto.common_service.impl.request;

import kz.lyalya.sto.common_data.enumeration.CarState;
import kz.lyalya.sto.common_service.impl.car.CarBelongsToClientChecker;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.EntityBelongsChecker;
import kz.lyalya.sto.dto.request.RequestCreateDTO;
import kz.lyalya.sto.dto.request.RequestDTO;
import kz.lyalya.sto.dto.request.RequestSearchDTO;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_data.repository.RequestRepository;
import kz.lyalya.sto.common_service.intf.car.CarService;
import kz.lyalya.sto.common_service.intf.request.RequestService;
import kz.lyalya.sto.common_service.intf.user.ClientService;
import kz.lyalya.sto.common_service.intf.user.MasterService;
import kz.lyalya.sto.exception.*;
import kz.lyalya.sto.util.validator.SearchModelCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {
    private final RequestRepository requestRepository;
    private final ClientService clientService;
    private final MasterService masterService;
    private final CarService carService;
    private final ModelSpecification<Request, RequestSearchDTO> specification;
    private EntityBelongsChecker entityBelongsChecker;
    private final SearchModelCheckService searchModelCheckService;

    @Override
    public Request findOne(long id) {
        return requestRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Заявка с id " + id + " не найдена в системе");
        });
    }

    @Override
    @Transactional
    public RequestDTO add(RequestCreateDTO dto, String email) {
        Client client = clientService.findOne(email);
        Master master = masterService.findOne(dto.getMasterId());
        Car car = carService.findOne(dto.getCarId());

        entityBelongsChecker = new CarBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(car, email)) {
            throw new CarDoesNotBelongsToClientException(car.getMark().toString() + " " + car.getModel(),
                                                        client.getUser().getName() + " " + client.getUser().getSurname());
        }
        Request request = Request.builder()
                .client(client)
                .master(master)
                .car(car)
                .problemDescription(dto.getProblemDescription())
                .build();
        request = requestRepository.save(request);
        return RequestDTO.convertToDto(request);
    }

    @Override
    @Transactional
    public RequestDTO update(RequestCreateDTO dto, String email) {
        Request request = findOne(dto.getId());

        entityBelongsChecker = new RequestBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(request, email)) {
            throw new OtherClientException("Заявку " + dto.getId() + " может обрабатывать только ее создатель");
        }

        Client client = clientService.findOne(email);
        Car car = carService.findOne(dto.getCarId());

        entityBelongsChecker = new CarBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(car, email)) {
            throw new CarDoesNotBelongsToClientException(car.getMark().toString() + " " + car.getModel(),
                    client.getUser().getName() + " " + client.getUser().getSurname());
        }
        Master master = masterService.findOne(dto.getMasterId());

        request.setCar(car);
        request.setMaster(master);
        request.setProblemDescription(dto.getProblemDescription());
        request = requestRepository.save(request);

        return RequestDTO.convertToDto(request);
    }

    @Override
    public Request changeState(long id, RequestState state) {
        Request request = findOne(id);
        request.setState(state);
        if (request.getState().equals(RequestState.IN_PROCESS)) {
            carService.changeState(request.getCar().getId(), CarState.REPAIRING);
            request.setDateOfStart(LocalDate.now());
        } else if (request.getState().equals(RequestState.CREATED)) {
            carService.changeState(request.getCar().getId(), CarState.NEW);
            request.setDateOfStart(null);
        }
        return requestRepository.save(request);
    }

    @Override
    @Transactional
    public void delete(long id, String email) {
        Request request = findOne(id);

        entityBelongsChecker = new RequestBelongsToClientChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(request, email)) {
            throw new OtherClientException("Заявку " + id + " может обрабатывать только ее создатель");
        }

        if (request.getState() == RequestState.IN_PROCESS || request.getState() == RequestState.FINISHED) {
            throw new EntityDeleteException("Невозможно удалить действующую " +
                    "или завершенную заявку");
        }
        requestRepository.delete(request);
    }

    @Override
    @Transactional
    public RequestDTO beginWork(String email, long id) {
        Request request = findOne(id);

        entityBelongsChecker = new RequestBelongsToMasterChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(request, email)) {
            throw new OtherMastersException("Работать на заявкой может только указанный мастер " + email);
        }

        request.setState(RequestState.IN_PROCESS);
        request.setDateOfStart(LocalDate.now());
        request = requestRepository.save(request);

        carService.changeState(request.getCar().getId(), CarState.REPAIRING);
        return RequestDTO.convertToDto(request);
    }

    @Override
    @Transactional
    public RequestDTO finishWork(String email, long id) {
        Request request = findOne(id);

        entityBelongsChecker = new RequestBelongsToMasterChecker();
        if (!entityBelongsChecker.checkEntityBelongsToUser(request, email)) {
            throw new OtherMastersException("Работать на заявкой может только указанный мастер " + email);
        }
        if (request.getDoneWorks().size() == 0) {
            throw new RequestInWorkException("Не найдено ни одной записи о проделанной работе по заявке" + id +
                    ". Пожалуйста, внесите данные");
        }
        if (request.getState().equals(RequestState.FINISHED)) {
            throw new RequestWasFinishedException(id);
        }
        request.setState(RequestState.FINISHED);
        request.setDateOfFinish(LocalDate.now());
        request.setPrice(countTotalPrice(request));
        request = requestRepository.save(request);

        carService.changeState(request.getCar().getId(), CarState.DONE);
        return RequestDTO.convertToDto(request);
    }

    @Override
    @Transactional
    public Page<RequestDTO> search(SearchModel<RequestSearchDTO> searchModel) {
        searchModel = searchModelCheckService.checkSearchModel(searchModel);
        Specification<Request> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        Page<Request> requests = requestRepository.findAll(specification, searchModel.getPagination().getPageRequest());
        List<RequestDTO> models = requests.stream().map(RequestDTO::convertToDto).collect(Collectors.toList());
        return new PageImpl<>(models, requests.getPageable(), requests.getTotalElements());
    }

    @Override
    public RequestDTO getOne(long id) {
        return RequestDTO.convertToDto(findOne(id));
    }

    @Override
    public Request save(Request request) {
        return requestRepository.save(request);
    }

    public int countTotalPrice(Request request) {
        return request.getDoneWorks().stream().mapToInt(DoneWork::getPrice).sum();
    }

    public void updateCurrentPrice(Request request, int addedPrice, int deletedPrice) {
        request.setCurrentPrice(request.getCurrentPrice() + addedPrice);
        save(request);
    }
}
