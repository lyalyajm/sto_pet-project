package kz.lyalya.sto.common_service.impl.car;

import kz.lyalya.sto.common_data.entity.car.CarPhoto;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.enumeration.CarState;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.common_service.impl.user.ClientServiceImpl;
import kz.lyalya.sto.dto.car.CarCreateDTO;
import kz.lyalya.sto.dto.car.CarDTO;
import kz.lyalya.sto.dto.car.CarSearchDTO;
import kz.lyalya.sto.dto.car.CarUpdateDTO;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_data.repository.CarRepository;
import kz.lyalya.sto.common_service.intf.car.CarService;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.util.bean.ModelMapperBean;
import kz.lyalya.sto.util.validator.SearchModelCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final ModelMapperBean modelMapperBean;
    private final CarPhotoServiceImpl carPhotoService;
    private final ModelSpecification<Car, CarSearchDTO> specification;
    private final ClientServiceImpl clientService;
    private final SearchModelCheckService searchModelCheckService;

    @Override
    public Car findOne(long id) {
        return carRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Автомобиль с id " + id + " не найден в системе");
        });
    }

    @Override
    public CarDTO add(CarCreateDTO dto, MultipartFile file, String email) {
        //надо чекнуть дефолтные поля создаются или нет
        CarDTO carDTO = add(dto, email);
        Car car = findOne(carDTO.getId());

        carPhotoService.add(file, car, true);

        return CarDTO.convertToDto(car);
    }

    @Override
    public CarDTO add(CarCreateDTO dto, String email) {
        Car car = modelMapperBean.modelMapper().map(dto, Car.class);
        car.setOwner(clientService.findOne(email));

        return CarDTO.convertToDto(carRepository.save(car));
    }

    @Override
    public Car changeState(long id, CarState state) {
        Car car = findOne(id);
        car.setState(state);

        return carRepository.save(car);
    }

    @Override
    public void delete(long id) {
        Car car = findOne(id);
        car.setDeleted(true);
        if (car.getState().equals(CarState.NEW) && checkCarRequestsStates(car.getRequests())) {
            car.getPhotos().forEach(ph -> carPhotoService.deletePhoto(car.getId(), ph.getFilePath()));
            carRepository.delete(car);
            return;
        }
        car.setDeleted(true);
        carRepository.save(car);
    }

    @Override
    public CarDTO update(CarUpdateDTO dto) {
        Car oldCar = findOne(dto.getId());
        oldCar.setMark(dto.getMark());
        oldCar.setModel(dto.getModel());
        oldCar.setYearOfProduce(dto.getYearOfProduce());
        oldCar = carRepository.save(oldCar);

        return CarDTO.convertToDto(oldCar);
    }

    @Override
    public Page<CarDTO> search(SearchModel<CarSearchDTO> searchModel) {
        searchModel = searchModelCheckService.checkSearchModel(searchModel);
        Specification<Car> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        Page<Car> cars = carRepository.findAll(specification, searchModel.getPagination().getPageRequest());
        List<CarDTO> models = cars.stream().map(CarDTO::convertToDto).collect(Collectors.toList());
        return new PageImpl<>(models, cars.getPageable(), cars.getTotalElements());
    }

    @Override
    public CarDTO getOne(long id) {
        return CarDTO.convertToDto(findOne(id));
    }

    @Override
    public CarDTO updatePhoto(long id, MultipartFile file, boolean isMainPhoto) {
        CarPhoto carPhoto = carPhotoService.updatePhoto(findOne(id), file, isMainPhoto);
        carPhotoService.checkMainPhoto(id);

        return getOne(id);
    }

    @Override
    public CarDTO deletePhoto(long id, String filePath) {
        carPhotoService.findByFilePathAndCarId(id, filePath);

        carPhotoService.deletePhoto(id, filePath);
        carPhotoService.checkMainPhoto(id);

        return getOne(id);
    }

    private boolean checkCarRequestsStates(List<Request> requests) {
        for (int i = 0; i < requests.size(); i++) {
            if (!requests.get(i).getState().equals(RequestState.CREATED)) {
                return false;
            }
        }
        return true;
    }
}
