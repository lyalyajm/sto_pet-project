package kz.lyalya.sto.common_service.impl.car;

import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.car.CarPhoto;
import kz.lyalya.sto.common_data.repository.CarPhotoRepository;
import kz.lyalya.sto.common_service.intf.car.CarPhotoService;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.exception.IncorrectFileFormatException;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import kz.lyalya.sto.util.file_upload_service.FilenameService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarPhotoServiceImpl implements CarPhotoService {
    private final CarPhotoRepository carPhotoRepository;
    private final FileUploadService fileUploadService;
    private final FilenameService filenameService;
    private final String entityName = "CAR";

    @Value("${files_cars_upload}")
    private String root;

    @Override
    @Transactional
    public CarPhoto add(MultipartFile file, Car car, boolean isMainPhoto) {
        String filename = filenameService.createFilename(entityName, car.getId(), file.getOriginalFilename());
        if (!filenameService.checkFileType(filename)) {
            throw new IncorrectFileFormatException();
        }
        CarPhoto carPhoto = CarPhoto.builder()
                .filePath(filename)
                .car(car)
                .mainPhoto(isMainPhoto)
                .build();
        fileUploadService.store(file, root, filename);
        carPhoto = carPhotoRepository.save(carPhoto);
        return carPhoto;
    }

    @Override
    public void deletePhoto(long carId, String filename) {
        fileUploadService.deleteFile(filename, root);
        CarPhoto carPhoto = findByFilePathAndCarId(carId, filename);
        carPhotoRepository.delete(carPhoto);
    }

    @Override
    @Transactional
    public CarPhoto updatePhoto(Car car, MultipartFile file, boolean isMainPhoto) {
        if (isMainPhoto) {
            carPhotoRepository.findByCarIdAndMainPhoto(car.getId(), true).ifPresent(userPhoto -> deletePhoto(car.getId(), userPhoto.getFilePath()));
        }
        return add(file, car, isMainPhoto);
    }

    @Override
    public CarPhoto findByFilePathAndCarId(long carId, String filePath) {
        return carPhotoRepository.findByFilePathAndCarId(filePath, carId)
            .orElseThrow(() ->{
                return new EntityNotFoundException("Фото с наименованием " + filePath + " не найдено в системе");
        });
    }

    @Override
    public boolean existsByFilePathAndCarId(long carId, String filePath) {
        return carPhotoRepository.existsByFilePathAndCarId(filePath, carId);
    }

    @Transactional
    public void checkMainPhoto(long carId) {
        List<CarPhoto> carPhotos = carPhotoRepository.findByCarId(carId);
        long mainPhoto = carPhotos.stream().filter(CarPhoto::isMainPhoto).count();
        if (mainPhoto == 0 && carPhotos.size() > 0) {
            carPhotos.get(0).setMainPhoto(true);
            carPhotoRepository.save(carPhotos.get(0));
        }
    }
}
