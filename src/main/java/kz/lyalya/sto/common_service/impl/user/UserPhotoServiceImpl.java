package kz.lyalya.sto.common_service.impl.user;

import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.entity.user.UserPhoto;
import kz.lyalya.sto.common_data.repository.UserPhotoRepository;
import kz.lyalya.sto.common_service.intf.user.UserPhotoService;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.exception.IncorrectFileFormatException;
import kz.lyalya.sto.util.FileUploadToServerThread;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import kz.lyalya.sto.util.file_upload_service.FilenameService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserPhotoServiceImpl implements UserPhotoService {
    private final FileUploadService fileUploadService;
    private final UserPhotoRepository userPhotoRepository;
    private final FilenameService filenameService;
    private final String entityName = "USER";

    @Value("${files_users_upload}")
    private String root;

    @Transactional
    @Override
    public UserPhoto add(MultipartFile file, User user, boolean isMainPhoto) {
        String filename = filenameService.createFilename(entityName, user.getId(), file.getOriginalFilename());
        if (!filenameService.checkFileType(filename)) {
            throw new IncorrectFileFormatException();
        }
        UserPhoto userPhoto = UserPhoto.builder()
                .filePath(filename)
                .user(user)
                .mainPhoto(isMainPhoto)
                .build();
//                fileUploadService.store(file, root, filename);

        FileUploadToServerThread thread = new FileUploadToServerThread(fileUploadService, file, root, filename);
        thread.run();

        userPhoto = userPhotoRepository.save(userPhoto);
        return userPhoto;
    }

    @Override
    public void deletePhoto(long userId, String filename) {
        UserPhoto userPhoto = findByFilePathAndUserId(userId, filename);
        fileUploadService.deleteFile(filename, root);
        userPhotoRepository.delete(userPhoto);
    }

    @Override
    public UserPhoto updatePhoto(MultipartFile file, User user, boolean isMainPhoto) {
        if (isMainPhoto) {
            userPhotoRepository.findByUserIdAndMainPhoto(user.getId(), true).ifPresent(userPhoto -> deletePhoto(user.getId(), userPhoto.getFilePath()));
        }
        return add(file, user, isMainPhoto);
    }

    @Override
    public UserPhoto findByFilePathAndUserId(long userId, String filePath) {
        return userPhotoRepository.findByFilePathAndUserId(filePath, userId)
                .orElseThrow(() -> {
            return new EntityNotFoundException("Фото с наименованием " + filePath + " не найдено в системе");
        });
    }

    @Override
    public boolean existsByFilePathAndUserId(long userId, String filePath) {
        return userPhotoRepository.existsByFilePathAndUserId(filePath, userId);
    }

    @Override
    public void deleteAllPhotosByUser(long userId) {
        List<UserPhoto> userPhotos = userPhotoRepository.findByUserId(userId);
        userPhotos.forEach(p -> deletePhoto(userId, p.getFilePath()));
    }


    public void checkMainPhoto(long userId) {
        List<UserPhoto> userPhotos = userPhotoRepository.findByUserId(userId);
        long mainPhoto = userPhotos.stream().filter(UserPhoto::isMainPhoto).count();
        if (mainPhoto == 0 && userPhotos.size() > 0) {
            userPhotos.get(0).setMainPhoto(true);
            userPhotoRepository.save(userPhotos.get(0));
        }
    }

//    @Transactional
//    public void checkMainPhoto(long carId) {
//        List<CarPhoto> carPhotos = carPhotoRepository.findByCarId(carId);
//        long mainPhoto = carPhotos.stream().filter(CarPhoto::isMainPhoto).count();
//        if (mainPhoto == 0 && carPhotos.size() > 0) {
//            carPhotos.get(0).setMainPhoto(true);
//            carPhotoRepository.save(carPhotos.get(0));
//        }
//    }
}

