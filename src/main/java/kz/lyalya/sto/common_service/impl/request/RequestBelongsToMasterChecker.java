package kz.lyalya.sto.common_service.impl.request;

import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.EntityBelongsChecker;
import org.springframework.stereotype.Service;

@Service
public class RequestBelongsToMasterChecker implements EntityBelongsChecker {

    @Override
    public boolean checkEntityBelongsToUser(Checkable entity, String userEmail) {
        Request request = (Request) entity;
        return request.getMaster().getUser().getEmail().equals(userEmail);
    }
}
