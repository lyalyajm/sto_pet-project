package kz.lyalya.sto.common_service.impl.user;

import kz.lyalya.sto.common_data.entity.user.Role;
import kz.lyalya.sto.common_data.enumeration.UserRole;
import kz.lyalya.sto.common_data.repository.RoleRepository;
import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.dto.master.MasterCreateDTO;
import kz.lyalya.sto.dto.master.MasterUpdateDTO;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.repository.UserRepository;
import kz.lyalya.sto.common_service.intf.user.UserService;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.exception.IncorrectPasswordException;
import kz.lyalya.sto.util.bean.ModelMapperBean;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserPhotoServiceImpl userPhotoService;
    private final ModelMapperBean modelMapper;
    private final BCryptPasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final UserRole MASTER_ROLE = UserRole.MASTER;
    private final UserRole CLIENT_ROLE = UserRole.CLIENT;

    @Override
    public User findOne(long id) {
        return userRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Пользователь с id " + id + " не найден в системе");
        });
    }

    @Override
    public User findOne(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> {
            return new EntityNotFoundException("Пользователь с email " + email + " не найден в системе");
        });
    }

    @Override
    public User add(ClientCreateDTO dto, MultipartFile file) {
        User user = add(dto);
        userPhotoService.add(file, user, true);

        return user;
    }

    @Override
    public User add(ClientCreateDTO dto) {
        User user = modelMapper.modelMapper().map(dto, User.class);
        Role role = findByName(CLIENT_ROLE);
        user.getRoles().add(role);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public User add(MasterCreateDTO dto, MultipartFile file) {
        User user = add(dto);
        userPhotoService.add(file, user, true);

        return user;
    }

    @Override
    public User add(MasterCreateDTO dto) {
        User user = modelMapper.modelMapper().map(dto, User.class);
        Role role = findByName(MASTER_ROLE);
        user.getRoles().add(role);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public void delete(long id) {
        User user = findOne(id);

        userPhotoService.deleteAllPhotosByUser(id);
        userRepository.delete(user);
    }

    @Override
    public void disable(long id) {
        User user = findOne(id);
        user.setDeleted(true);
        user.setEnabled(false);
        userRepository.save(user);
    }

    @Override
    public User update(ClientUpdateDTO dto, long id) {
        User oldUser = findOne(id);
        oldUser.setName(dto.getName());
        oldUser.setSurname(dto.getSurname());
        oldUser.setDateOfBirth(dto.getDateOfBirth());
        oldUser.setPhoneNumber(dto.getPhoneNumber());
        oldUser.setEmail(dto.getEmail());
        return userRepository.save(oldUser);
    }

    @Override
    public User update(MasterUpdateDTO dto, long id) {
        User oldUser = findOne(id);
        oldUser.setName(dto.getName());
        oldUser.setSurname(dto.getSurname());
        oldUser.setDateOfBirth(dto.getDateOfBirth());
        oldUser.setPhoneNumber(dto.getPhoneNumber());
        oldUser.setEmail(dto.getEmail());
        return userRepository.save(oldUser);
    }

    @Override
    public User updatePassword(UserPasswordUpdateDTO dto, long id) {
        User user = findOne(id);
        if (!passwordEncoder.matches(dto.getCurrentPassword(), user.getPassword())) {
            throw new IncorrectPasswordException("Неверный пароль");
        }
        user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
        return userRepository.save(user);
    }

    @Override
    public User updatePhoto(User user, MultipartFile file, boolean isMainPhoto) {
        userPhotoService.updatePhoto(file, user, isMainPhoto);
        userPhotoService.checkMainPhoto(user.getId());
        return user;
    }

    @Override
    public User deletePhoto(User user, String filePath) {
        userPhotoService.deletePhoto(user.getId(), filePath);
        userPhotoService.checkMainPhoto(user.getId());
        return findOne(user.getId());
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    private Role findByName(UserRole name) {
        return roleRepository.findByRole(name).orElseThrow(() -> {
            return new EntityNotFoundException("Роль " + name + " не найдена в системе");
        });
    }
}
