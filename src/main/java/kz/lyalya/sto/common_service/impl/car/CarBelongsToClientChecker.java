package kz.lyalya.sto.common_service.impl.car;

import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.EntityBelongsChecker;
import org.springframework.stereotype.Service;


@Service
public class CarBelongsToClientChecker implements EntityBelongsChecker {

    @Override
    public boolean checkEntityBelongsToUser(Checkable entity, String userEmail) {
        Car car = (Car) entity;
        return car.getOwner().getUser().getEmail().equals(userEmail);
    }
}
