package kz.lyalya.sto.common_service.impl.review;

import kz.lyalya.sto.common_data.entity.review.ReviewPhoto;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.dto.review.ReviewCreateDTO;
import kz.lyalya.sto.dto.review.ReviewDTO;
import kz.lyalya.sto.dto.review.ReviewSearchDTO;
import kz.lyalya.sto.dto.review.ReviewUpdateDTO;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_data.repository.ReviewRepository;
import kz.lyalya.sto.common_service.impl.request.RequestServiceImpl;
import kz.lyalya.sto.common_service.intf.review.ReviewService;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.exception.IncorrectFileFormatException;
import kz.lyalya.sto.exception.RequestInWorkException;
import kz.lyalya.sto.util.bean.ModelMapperBean;
import kz.lyalya.sto.util.validator.SearchModelCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class ReviewServiceImpl implements ReviewService {
    private final ReviewRepository reviewRepository;
    private final RequestServiceImpl requestService;
    private final ModelMapperBean modelMapperBean;
    private final ReviewPhotoServiceImpl reviewPhotoService;
    private final ModelSpecification<Review, ReviewSearchDTO> specification;
    private final SearchModelCheckService<ReviewSearchDTO> searchModelCheckService;

    @Override
    public Review findOne(long id) {
        return reviewRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Отзыв с id " + id + " не был найден в системе");
        });
    }

    @Override
    public ReviewDTO add(ReviewCreateDTO dto) {
        Request request = requestService.findOne(dto.getRequestId());
        if (!request.getState().equals(RequestState.FINISHED)) {
            throw new RequestInWorkException("Невозможно оценить заявку " + dto.getRequestId() + ", т.к. она все еще в процессе работы");
        }
        Review review = modelMapperBean.modelMapper().map(dto, Review.class);
        review.setRequest(request);
        review = reviewRepository.save(review);
        return ReviewDTO.convertToDto(review);
    }

    @Override
    public ReviewDTO add(ReviewCreateDTO dto, MultipartFile file) {
        ReviewDTO reviewDTO = add(dto);
        Review review = findOne(reviewDTO.getId());
        reviewPhotoService.add(file, review);
        return ReviewDTO.convertToDto(review);
    }

    @Override
    public ReviewDTO add(ReviewCreateDTO dto, MultipartFile file, MultipartFile file2) {
        ReviewDTO review = add(dto, file);
        long id = review.getId();
        reviewPhotoService.add(file2, findOne(id));
        return getOne(id);
    }

    @Override
    public ReviewDTO add(ReviewCreateDTO dto, MultipartFile file, MultipartFile file2, MultipartFile file3) {
        ReviewDTO review = add(dto, file, file2);
        long id = review.getId();
        reviewPhotoService.add(file3, findOne(id));
        return getOne(id);
    }

    @Override
    public void delete(long id) {
        Review review = findOne(id);
        List<ReviewPhoto> photos = review.getPhotos();
        if (!photos.isEmpty() && photos.size() > 0) {
            photos.forEach(ph -> reviewPhotoService.deletePhoto(id, ph.getFilePath()));
        }
        reviewRepository.delete(findOne(id));
    }

    @Override
    public ReviewDTO update(ReviewUpdateDTO dto) {
        Review review = findOne(dto.getId());
        review.setComment(dto.getComment());
        review.setRate(dto.getRate());
        review = reviewRepository.save(review);
        return ReviewDTO.convertToDto(review);
    }

    @Override
    public Page<ReviewDTO> search(SearchModel<ReviewSearchDTO> searchModel) {
        searchModel = searchModelCheckService.checkSearchModel(searchModel);
        Specification<Review> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        Page<Review> page = reviewRepository.findAll(specification, searchModel.getPagination().getPageRequest());
        List<ReviewDTO> dtos = page.stream().map(ReviewDTO::convertToDto).collect(Collectors.toList());
        return new PageImpl<>(dtos, page.getPageable(), page.getTotalElements());
    }

    @Override
    public ReviewDTO getOne(long id) {
        return ReviewDTO.convertToDto(findOne(id));
    }

    @Override
    public ReviewDTO updatePhoto(long id, MultipartFile file) {
        Review review = findOne(id);
        reviewPhotoService.add(file, review);
        return ReviewDTO.convertToDto(review);
    }

    @Override
    public ReviewDTO deletePhoto(long id, String filePath) {
        reviewPhotoService.deletePhoto(id, filePath);
        return getOne(id);
    }

    @Override
    public boolean checkFilenames(String filename) {
        return reviewPhotoService.getFilenameService().checkFileType(filename);
    }

    @Override
    public boolean checkFilenames(String filename, String filename2) {
        return reviewPhotoService.getFilenameService().checkFileType(filename) &&
                reviewPhotoService.getFilenameService().checkFileType(filename2);
    }

    @Override
    public boolean checkFilenames(String filename, String filename2, String filename3) {
        if (reviewPhotoService.getFilenameService().checkFileType(filename) &&
                reviewPhotoService.getFilenameService().checkFileType(filename2) &&
                reviewPhotoService.getFilenameService().checkFileType(filename3)) {
            return true;
        }
        throw new IncorrectFileFormatException();
    }
}
