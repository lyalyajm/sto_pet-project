package kz.lyalya.sto.common_service.impl.review;

import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.entity.review.ReviewPhoto;
import kz.lyalya.sto.common_data.repository.ReviewPhotoRepository;
import kz.lyalya.sto.common_service.intf.review.ReviewPhotoService;
import kz.lyalya.sto.exception.EntityDeleteException;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.exception.IncorrectFileFormatException;
import kz.lyalya.sto.util.FileUploadToServerThread;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import kz.lyalya.sto.util.file_upload_service.FilenameService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class ReviewPhotoServiceImpl implements ReviewPhotoService {
    private final ReviewPhotoRepository reviewPhotoRepository;
    private final FileUploadService fileUploadService;
    private final FilenameService filenameService;
    private final String entityName = "REVIEW";

    @Value("${files_reviews_upload}")
    private String root;

    @Override
    public ReviewPhoto add(MultipartFile file, Review review) {
        String filename = filenameService.createFilename(entityName, review.getId(), file.getOriginalFilename());
        if (!filenameService.checkFileType(filename)) {
            throw new IncorrectFileFormatException();
        }
        ReviewPhoto reviewPhoto = ReviewPhoto.builder()
                .filePath(filename)
                .review(review)
                .build();
//        fileUploadService.store(file, root, filename);

        FileUploadToServerThread thread = new FileUploadToServerThread(fileUploadService, file, root, filename);
        thread.run();

        reviewPhoto = reviewPhotoRepository.save(reviewPhoto);
        return reviewPhoto;
    }

    @Override
    public boolean deletePhoto(long reviewId, String filename) {
        boolean deleted = fileUploadService.deleteFile(filename, root);
        if (!deleted) {
            throw new EntityDeleteException("Не удалось удалить фото " + filename);
        }
        ReviewPhoto reviewPhoto = findByFilePathAndReviewId(reviewId, filename);
        reviewPhotoRepository.delete(reviewPhoto);
        return existsByFilePathAndReviewId(reviewId, filename);
    }

    @Override
    public ReviewPhoto findByFilePathAndReviewId(long reviewId, String filePath) {
        return reviewPhotoRepository.findByFilePathAndReviewId(filePath, reviewId)
            .orElseThrow(() ->{
                return new EntityNotFoundException("Фото с наименованием " + filePath + " не найдено в системе");
        });
    }

    @Override
    public boolean existsByFilePathAndReviewId(long reviewId, String filePath) {
        return reviewPhotoRepository.existsByFilePathAndReviewId(filePath, reviewId);
    }

    public FilenameService getFilenameService() {
        return filenameService;
    }
}
