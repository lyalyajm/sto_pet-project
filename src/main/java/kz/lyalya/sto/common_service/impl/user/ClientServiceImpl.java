package kz.lyalya.sto.common_service.impl.user;

import kz.lyalya.sto.common_data.repository.CarRepository;
import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientDTO;
import kz.lyalya.sto.dto.client.ClientSearchDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_data.repository.ClientRepository;
import kz.lyalya.sto.common_service.intf.user.ClientService;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.util.validator.SearchModelCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final UserServiceImpl userService;
    private final ModelSpecification<Client, ClientSearchDTO> specification;
    private final SearchModelCheckService<ClientSearchDTO> searchModelCheckService;
    private final CarRepository carRepository;

    @Override
    public Client findOne(long id) {
        return clientRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Клиент с id " + id + " не найден в системе");
        });
    }

    @Override
    public Client findOne(String email) {
        return clientRepository.findByUserEmail(email).orElseThrow(() -> {
            return new EntityNotFoundException("Клиент с email " + email + " не найден в системе");
        });
    }

    @Override
    public ClientDTO add(ClientCreateDTO dto, MultipartFile file) {
        User user = userService.add(dto, file);

        Client client = Client.builder()
                .user(user)
                .build();
        client = clientRepository.save(client);
        return ClientDTO.convertToDto(client);
    }

    @Override
    public ClientDTO add(ClientCreateDTO dto) {
        User user = userService.add(dto);

        Client client = Client.builder()
                .user(user)
                .build();
        client = clientRepository.save(client);
        return ClientDTO.convertToDto(client);
    }

    @Override
    public boolean delete(long id) {
        Client client = findOne(id);
        if ((client.getCars() == null || client.getCars().size() == 0) &&
                (client.getRequests() == null || client.getRequests().size() == 0)) {
            clientRepository.delete(client);
            userService.delete(client.getUser().getId());
            return true;
        } else if ((client.getRequests() == null || client.getRequests().size() == 0)) {
            carRepository.deleteAllByOwnerId(id);
            clientRepository.delete(client);
            userService.delete(client.getUser().getId());
            return true;
        } else {
            userService.disable(client.getUser().getId());
        }
        return true;
    }

    @Override
    public ClientDTO update(ClientUpdateDTO dto, String email) {
        Client client = findOne(email);
        userService.update(dto, client.getUser().getId());
        return ClientDTO.convertToDto(findOne(client.getId()));
    }

    @Override
    public ClientDTO updatePassword(UserPasswordUpdateDTO dto, long id, String email) {
        Client client = findOne(email);
        userService.updatePassword(dto, client.getUser().getId());
        return getOne(id);
    }

    @Override
    public Page<ClientDTO> search(SearchModel<ClientSearchDTO> searchModel) {
        searchModel = searchModelCheckService.checkSearchModel(searchModel);
        Specification<Client> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        Page<Client> clients = clientRepository.findAll(specification, searchModel.getPagination().getPageRequest());
        List<ClientDTO> models = clients.stream().map(ClientDTO::convertToDto).collect(Collectors.toList());
        return new PageImpl<>(models, clients.getPageable(), clients.getTotalElements());
    }

    @Override
    public ClientDTO getOne(long id) {
        return ClientDTO.convertToDto(findOne(id));
    }

    @Override
    public ClientDTO updatePhoto(long id, MultipartFile file, boolean isMainPhoto, String email) {
        userService.updatePhoto(findOne(email).getUser(), file, isMainPhoto);
        return getOne(id);
    }

    @Override
    public ClientDTO deletePhoto(long id, String filePath, String email) {
        userService.deletePhoto(findOne(email).getUser(), filePath);
        return getOne(id);
    }

    @Override
    public boolean checkCurrentUser(String email, long id) {
        return findOne(id).getUser().getEmail().equals(email);
    }
}
