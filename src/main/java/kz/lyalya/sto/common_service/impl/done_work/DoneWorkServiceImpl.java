package kz.lyalya.sto.common_service.impl.done_work;

import kz.lyalya.sto.common_data.enumeration.WorkType;
import kz.lyalya.sto.dto.done_work.DoneWorkCreateDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkSearchDTO;
import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_data.repository.DoneWorkRepository;
import kz.lyalya.sto.common_service.impl.request.RequestServiceImpl;
import kz.lyalya.sto.common_service.intf.done_works.DoneWorkService;
import kz.lyalya.sto.dto.done_work.DoneWorkUpdateDTO;
import kz.lyalya.sto.exception.EntityDeleteException;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.util.bean.ModelMapperBean;
import kz.lyalya.sto.util.validator.SearchModelCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DoneWorkServiceImpl implements DoneWorkService {
    private final DoneWorkRepository doneWorkRepository;
    private final RequestServiceImpl requestService;
    private final ModelMapperBean modelMapper;
    private final ModelSpecification<DoneWork, DoneWorkSearchDTO> specification;
    private final DoneWorkBelongsToMasterChecker doneWorkChecker;
    private final SearchModelCheckService searchModelCheckService;

    @Override
    public DoneWork findOne(long id) {
        return doneWorkRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Работы с id " + id + " не были найдены в системе");
        });
    }

    @Override
    public DoneWorkDTO add(DoneWorkCreateDTO dto) {
        Request request = requestService.changeState(dto.getRequestId(), RequestState.IN_PROCESS);

        DoneWork doneWork = DoneWork.builder()
                .request(request)
                .workType(WorkType.valueOf(dto.getWorkType()))
                .note(dto.getNote())
                .price(dto.getPrice())
                .date(dto.getDate() == null ? LocalDate.now() : dto.getDate())
                .build();
        doneWork = doneWorkRepository.save(doneWork);

        return DoneWorkDTO.convertToDto(doneWork);
    }

    @Override
    @Transactional
    public void delete(long id) {
        DoneWork doneWork = findOne(id);
        if (doneWork.getRequest().getState().equals(RequestState.FINISHED)) {
            throw new EntityDeleteException("Невозможно удалить запись, т.к. работа была выполнена");
        }
        if (doneWork.getRequest().getDoneWorks().size() == 1) {
            requestService.changeState(doneWork.getRequest().getId(), RequestState.CREATED);
        }
        doneWorkRepository.delete(doneWork);
    }

    @Override
    @Transactional
    public DoneWorkDTO update(DoneWorkUpdateDTO dto) {
        DoneWork doneWork = findOne(dto.getId());
        doneWork.setDate(dto.getDate() == null ? doneWork.getDate() : dto.getDate());
        doneWork.setPrice(dto.getPrice());
        doneWork.setNote(dto.getNote());
        doneWork.setWorkType(WorkType.valueOf(dto.getWorkType()));
        doneWork = doneWorkRepository.save(doneWork);

        return DoneWorkDTO.convertToDto(doneWork);
    }

    @Override
    @Transactional
    public Page<DoneWorkDTO> search(SearchModel<DoneWorkSearchDTO> searchModel) {
        searchModel = searchModelCheckService.checkSearchModel(searchModel);
        Specification<DoneWork> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        Page<DoneWork> doneWorks = doneWorkRepository.findAll(specification, searchModel.getPagination().getPageRequest());
        List<DoneWorkDTO> dtos = doneWorks.stream().map(DoneWorkDTO::convertToDto).collect(Collectors.toList());

        return new PageImpl<>(dtos, doneWorks.getPageable(), doneWorks.getTotalElements());
    }

    @Override
    public DoneWorkDTO getOne(long id) {
        return DoneWorkDTO.convertToDto(findOne(id));
    }
}
