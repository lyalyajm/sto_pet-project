package kz.lyalya.sto.common_service.impl.done_work;

import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.EntityBelongsChecker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class DoneWorkBelongsToMasterChecker implements EntityBelongsChecker {

    @Override
    public boolean checkEntityBelongsToUser(Checkable entity, String userEmail) {
        DoneWork doneWork = (DoneWork) entity;
        return doneWork.getRequest().getMaster().getUser().getEmail().equals(userEmail);
    }
}
