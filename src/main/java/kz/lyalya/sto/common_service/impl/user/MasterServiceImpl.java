package kz.lyalya.sto.common_service.impl.user;

import kz.lyalya.sto.dto.master.*;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.common_data.repository.MasterRepository;
import kz.lyalya.sto.common_service.intf.user.MasterService;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import kz.lyalya.sto.exception.EntityNotFoundException;
import kz.lyalya.sto.util.validator.SearchModelCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class MasterServiceImpl implements MasterService {
    private final MasterRepository masterRepository;
    private final UserServiceImpl userService;
    private final ModelSpecification<Master, MasterSearchDTO> specification;
    private final SearchModelCheckService searchModelCheckService;

    @Override
    public Master findOne(long id) {
        return masterRepository.findById(id).orElseThrow(() -> {
            return new EntityNotFoundException("Мастер с id " + id + " не найден в системе");
        });
    }

    @Override
    public Master findOne(String email) {
        return masterRepository.findByUserEmail(email).orElseThrow(() -> {
            return new EntityNotFoundException("Мастер с email " + email + " не найден в системе");
        });
    }

    //    @Secured("ADMIN")
    @Override
    public MasterDTO add(MasterCreateDTO dto, MultipartFile file, String email) {
        User creator = userService.findOne(email);
        User user = userService.add(dto, file);

        Master master = Master.builder()
                .user(user)
                .creator(creator)
                .experience(dto.getExperience())
                .description(dto.getDescription())
                .build();
        master = masterRepository.save(master);

        return MasterDTO.convertToDto(master);
    }

    @Override
    public MasterDTO add(MasterCreateDTO dto, String email) {
        User creator = userService.findOne(email);
        User user = userService.add(dto);

        Master master = Master.builder()
                .user(user)
                .creator(creator)
                .experience(dto.getExperience())
                .description(dto.getDescription())
                .build();
        master = masterRepository.save(master);

        return MasterDTO.convertToDto(master);
    }

    @Override
    public boolean delete(long id) {
        Master master = findOne(id);

        if (master.getRequests() == null || master.getRequests().size() == 0){
            masterRepository.delete(master);
            userService.delete(master.getUser().getId());
            return true;
        } else {
            userService.disable(master.getUser().getId());
        }
        return true;
    }

    @Override
    public MasterDTO update(MasterUpdateDTO dto) {
        Master master = findOne(dto.getId());
        master.setDescription(dto.getDescription());
        master.setExperience(dto.getExperience());
        userService.update(dto, master.getUser().getId());

        return MasterDTO.convertToDto(master);
    }

    @Override
    public MasterDTO updatePassword(UserPasswordUpdateDTO dto, long id) {
        Master master = findOne(id);
        userService.updatePassword(dto, master.getUser().getId());
        return getOne(id);
    }

    @Override
    public Page<MasterDTO> search(SearchModel<MasterSearchDTO> searchModel) {
        searchModel = searchModelCheckService.checkSearchModel(searchModel);
        Specification<Master> specification = this.specification.createSpecification(searchModel.getFilter(), searchModel.getSort());
        Page<Master> clients = masterRepository.findAll(specification, searchModel.getPagination().getPageRequest());
        List<MasterDTO> models = clients.stream().map(MasterDTO::convertToDto).collect(Collectors.toList());

        return new PageImpl<>(models, clients.getPageable(), clients.getTotalElements());
    }

    @Override
    public MasterDTO getOne(long id) {
        return MasterDTO.convertToDto(findOne(id));
    }

    @Override
    public MasterDTO changeState(MasterChangeStateDTO dto) {
        Master master = findOne(dto.getId());
        master.setWorkable(dto.isWorkable());
        master = masterRepository.save(master);

        return MasterDTO.convertToDto(master);
    }

    @Override
    public MasterDTO updatePhoto(long id, MultipartFile file, boolean isMainPhoto) {
        userService.updatePhoto(findOne(id).getUser(), file, isMainPhoto);
        return getOne(id);
    }

    @Override
    public MasterDTO deletePhoto(long id, String filePath) {
        userService.deletePhoto(findOne(id).getUser(), filePath);
        return getOne(id);
    }

    @Override
    public boolean checkCurrentUser(String email, long id) {
        return findOne(id).getUser().getEmail().equals(email);
    }
}
