package kz.lyalya.sto.common_service.intf.user;

import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientDTO;
import kz.lyalya.sto.dto.client.ClientSearchDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface ClientService {
    Client findOne(long id);
    Client findOne(String email);
    ClientDTO add(ClientCreateDTO dto, MultipartFile file);
    ClientDTO add(ClientCreateDTO dto);
    boolean delete(long id);
    ClientDTO update(ClientUpdateDTO dto, String email);
    ClientDTO updatePassword(UserPasswordUpdateDTO dto, long id, String email);
    Page<ClientDTO> search(SearchModel<ClientSearchDTO> searchModel);
    ClientDTO getOne(long id);
    ClientDTO updatePhoto(long id, MultipartFile file, boolean isMainPhoto, String email);
    ClientDTO deletePhoto(long id, String filePath, String email);
    boolean checkCurrentUser(String email, long id);
}
