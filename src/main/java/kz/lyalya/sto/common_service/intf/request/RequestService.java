package kz.lyalya.sto.common_service.intf.request;

import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.dto.request.RequestCreateDTO;
import kz.lyalya.sto.dto.request.RequestDTO;
import kz.lyalya.sto.dto.request.RequestSearchDTO;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import org.springframework.data.domain.Page;

public interface RequestService {
    Request findOne(long id);
    RequestDTO add(RequestCreateDTO dto, String email);
    RequestDTO update(RequestCreateDTO dto, String email);
    Request changeState(long id, RequestState state);
    void delete(long id, String email);
    RequestDTO beginWork(String email, long requestId);
    RequestDTO finishWork(String email, long requestId);
    Page<RequestDTO> search(SearchModel<RequestSearchDTO> searchModel);
    RequestDTO getOne(long id);
    Request save(Request request);
}
