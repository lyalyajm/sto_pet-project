package kz.lyalya.sto.common_service.intf.entity_belongs_checker;

public interface EntityBelongsChecker {
    boolean checkEntityBelongsToUser(Checkable entity, String userEmail);
}
