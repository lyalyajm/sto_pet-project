package kz.lyalya.sto.common_service.intf.schedule;

import kz.lyalya.sto.common_data.entity.schedule.Schedule;
import kz.lyalya.sto.dto.schedule.ScheduleCreateDTO;
import kz.lyalya.sto.dto.schedule.ScheduleDTO;
import kz.lyalya.sto.dto.schedule.ScheduleUpdateDTO;

public interface ScheduleService {
    Schedule findByRequest(long requestId);
    Schedule findOne(long id);
    ScheduleDTO add(ScheduleCreateDTO dto);
    void delete(long id);
    ScheduleDTO update(ScheduleUpdateDTO dto);

}
