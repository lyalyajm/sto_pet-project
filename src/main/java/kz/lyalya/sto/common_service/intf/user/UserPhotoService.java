package kz.lyalya.sto.common_service.intf.user;

import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.entity.user.UserPhoto;
import org.springframework.web.multipart.MultipartFile;

public interface UserPhotoService {
    UserPhoto add(MultipartFile file, User user, boolean isMainPhoto);
    void deletePhoto(long userId, String filename);
    UserPhoto updatePhoto(MultipartFile file, User user, boolean isMainPhoto);
    UserPhoto findByFilePathAndUserId(long userId, String filePath);
    boolean existsByFilePathAndUserId(long userId, String filePath);
    void deleteAllPhotosByUser(long userId);
}
