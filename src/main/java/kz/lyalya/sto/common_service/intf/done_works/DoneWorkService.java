package kz.lyalya.sto.common_service.intf.done_works;

import kz.lyalya.sto.dto.done_work.DoneWorkCreateDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkDTO;
import kz.lyalya.sto.dto.done_work.DoneWorkSearchDTO;
import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.dto.done_work.DoneWorkUpdateDTO;
import org.springframework.data.domain.Page;

public interface DoneWorkService {
    DoneWork findOne(long id);
    DoneWorkDTO add(DoneWorkCreateDTO dto);
    void delete(long id);
    DoneWorkDTO update(DoneWorkUpdateDTO dto);
    Page<DoneWorkDTO> search(SearchModel<DoneWorkSearchDTO> searchModel);
    DoneWorkDTO getOne(long id);
}
