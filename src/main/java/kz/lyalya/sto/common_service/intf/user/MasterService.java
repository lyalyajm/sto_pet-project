package kz.lyalya.sto.common_service.intf.user;

import kz.lyalya.sto.dto.master.*;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface MasterService {
    Master findOne(long id);
    Master findOne(String email);
    MasterDTO add(MasterCreateDTO dto, MultipartFile file, String email);
    MasterDTO add(MasterCreateDTO dto, String email);
    boolean delete(long id);
    MasterDTO update(MasterUpdateDTO dto);
    MasterDTO updatePassword(UserPasswordUpdateDTO dto, long id);
    Page<MasterDTO> search(SearchModel<MasterSearchDTO> searchModel);
    MasterDTO getOne(long id);
    MasterDTO changeState(MasterChangeStateDTO dto);
    MasterDTO updatePhoto(long id, MultipartFile file, boolean isMainPhoto);
    MasterDTO deletePhoto(long id, String filePath);
    boolean checkCurrentUser(String email, long id);
}

