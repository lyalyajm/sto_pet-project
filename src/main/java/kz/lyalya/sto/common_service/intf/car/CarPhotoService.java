package kz.lyalya.sto.common_service.intf.car;

import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.car.CarPhoto;
import org.springframework.web.multipart.MultipartFile;

public interface CarPhotoService {
    CarPhoto add(MultipartFile file, Car car, boolean isMainPhoto);
    void deletePhoto(long carId, String filename);
    CarPhoto updatePhoto(Car car, MultipartFile file, boolean isMainPhoto);
    CarPhoto findByFilePathAndCarId(long carId, String filePath);
    boolean existsByFilePathAndCarId(long carId, String filePath);
}
