package kz.lyalya.sto.common_service.intf.car;

import kz.lyalya.sto.common_data.enumeration.CarState;
import kz.lyalya.sto.dto.car.CarCreateDTO;
import kz.lyalya.sto.dto.car.CarDTO;
import kz.lyalya.sto.dto.car.CarSearchDTO;
import kz.lyalya.sto.dto.car.CarUpdateDTO;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface CarService {
    Car findOne(long id);
    CarDTO add(CarCreateDTO dto, MultipartFile file, String email);
    CarDTO add(CarCreateDTO dto, String email);
    Car changeState(long id, CarState state);
    void delete(long id);
    CarDTO update(CarUpdateDTO dto);
    Page<CarDTO> search(SearchModel<CarSearchDTO> searchModel);
    CarDTO getOne(long id);
    CarDTO updatePhoto(long id, MultipartFile file, boolean isMainPhoto);
    CarDTO deletePhoto(long id, String filePath);
}
