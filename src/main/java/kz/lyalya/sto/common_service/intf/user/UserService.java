package kz.lyalya.sto.common_service.intf.user;

import kz.lyalya.sto.dto.client.ClientCreateDTO;
import kz.lyalya.sto.dto.client.ClientUpdateDTO;
import kz.lyalya.sto.dto.master.MasterCreateDTO;
import kz.lyalya.sto.dto.master.MasterUpdateDTO;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.dto.user.UserPasswordUpdateDTO;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {
    User findOne(long id);
    User findOne(String email);
    User add(ClientCreateDTO dto, MultipartFile file);
    User add(ClientCreateDTO dto);
    User add(MasterCreateDTO dto, MultipartFile file);
    User add(MasterCreateDTO dto);
    void delete(long id);
    void disable(long id);
    User update(ClientUpdateDTO dto, long id);
    User update(MasterUpdateDTO dto, long id);
    User updatePassword(UserPasswordUpdateDTO dto, long id);
    User updatePhoto(User user, MultipartFile file, boolean isMainPhoto);
    User deletePhoto(User user, String filePath);
    boolean userExistsByEmail(String email);
}
