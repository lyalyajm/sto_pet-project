package kz.lyalya.sto.common_service.intf.review;

import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.entity.review.ReviewPhoto;
import org.springframework.web.multipart.MultipartFile;

public interface ReviewPhotoService {
    ReviewPhoto add(MultipartFile file, Review review);
    boolean deletePhoto(long reviewId, String filename);
    ReviewPhoto findByFilePathAndReviewId(long reviewId, String filePath);
    boolean existsByFilePathAndReviewId(long reviewId, String filePath);
}
