package kz.lyalya.sto.common_service.intf.review;

import kz.lyalya.sto.dto.review.ReviewCreateDTO;
import kz.lyalya.sto.dto.review.ReviewDTO;
import kz.lyalya.sto.dto.review.ReviewSearchDTO;
import kz.lyalya.sto.dto.review.ReviewUpdateDTO;
import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface ReviewService {
    Review findOne(long id);
    ReviewDTO add(ReviewCreateDTO dto);
    ReviewDTO add(ReviewCreateDTO dto, MultipartFile file);
    ReviewDTO add(ReviewCreateDTO dto, MultipartFile file, MultipartFile file2);
    ReviewDTO add(ReviewCreateDTO dto, MultipartFile file, MultipartFile file2, MultipartFile file3);
    void delete(long id);
    ReviewDTO update(ReviewUpdateDTO dto);
    Page<ReviewDTO> search(SearchModel<ReviewSearchDTO> searchModel);
    ReviewDTO getOne(long id);
    ReviewDTO updatePhoto(long id, MultipartFile file);
    ReviewDTO deletePhoto(long id, String filePath);
    boolean checkFilenames(String filename);
    boolean checkFilenames(String filename, String filename2);
    boolean checkFilenames(String filename, String filename2, String filename3);
}
