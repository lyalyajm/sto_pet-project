package kz.lyalya.sto.util;

import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@Getter
@Setter
public class FileUploadToServerThread implements Runnable {
    private FileUploadService fileUploadService;
    private MultipartFile file;
    private String root;
    private String fileName;

    @Override
    public void run() {
        fileUploadService.store(file, root, fileName);
    }
}
