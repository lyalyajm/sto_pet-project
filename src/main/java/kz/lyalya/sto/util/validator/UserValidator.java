package kz.lyalya.sto.util.validator;

import kz.lyalya.sto.common_service.intf.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@RequiredArgsConstructor
@Component
public class UserValidator implements Validator {
    private final UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(EmailValidatable.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        EmailValidatable user = (EmailValidatable) target;
        if (userService.userExistsByEmail(user.getUserEmail())) {
            errors.rejectValue("email", "0", "Данный email занят другим пользователем");
        }
    }
}
