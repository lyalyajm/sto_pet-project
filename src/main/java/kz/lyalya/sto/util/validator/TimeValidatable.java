package kz.lyalya.sto.util.validator;

import java.time.LocalDateTime;

public interface TimeValidatable {
    LocalDateTime getDateTimeOfStart();
    LocalDateTime getDateTimeOfFinish();
}
