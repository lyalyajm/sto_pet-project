package kz.lyalya.sto.util.validator;

import kz.lyalya.sto.exception.ScheduleTimeException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDateTime;

public class ScheduleValidator <T extends TimeValidatable> implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(TimeValidatable.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        TimeValidatable schedule = (TimeValidatable) target;
        if (schedule.getDateTimeOfStart().isBefore(LocalDateTime.now()) ||
                schedule.getDateTimeOfFinish().isBefore(LocalDateTime.now())) {
            throw new ScheduleTimeException("Указанное время вышло");
        }
        if (schedule.getDateTimeOfFinish().isBefore(schedule.getDateTimeOfStart())) {
            throw new ScheduleTimeException("Время окончания работы не может быть раньше времени начала");
        }
    }
}
