package kz.lyalya.sto.util.validator;

import kz.lyalya.sto.common_data.pagination_specification.common.FilterModel;
import kz.lyalya.sto.common_data.pagination_specification.common.SearchModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class SearchModelCheckService <T extends FilterModel>{
    @Value("${pagination.default_page_size}")
    private short defaultPageSize;

    @Value("${pagination.default_sort_direction}")
    private Sort.Direction defaultSortDirection;

    public SearchModel<T> checkSearchModel(SearchModel<T> searchModel) {
        if (searchModel.getPagination().getSize() == 0) {
            searchModel.getPagination().setSize(defaultPageSize);
        } else if(searchModel.getSort().getOrder() == null ||
                searchModel.getSort().getOrder().toString().equals("")) {
            searchModel.getSort().setOrder(defaultSortDirection);
        }
        return searchModel;
    }
}
