package kz.lyalya.sto.util.file_upload_service;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ConfigurationProperties("storage")
public class StorageProperties {

    @Value("${files_upload}")
    private String location;
}
