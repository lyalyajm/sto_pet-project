package kz.lyalya.sto.util.file_upload_service;


import kz.lyalya.sto.exception.EntityDeleteException;
import kz.lyalya.sto.exception.StorageException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
@RequiredArgsConstructor
public class FileUploadService {
    private final Path rootLocation;

    @Value("${files_users_upload}")
    private String usersFilesPath;

    @Value("${files_cars_upload}")
    private String carsFilesPath;

    @Value("${files_reviews_upload}")
    private String reviewsFilesPath;

    @Autowired
    public FileUploadService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    public void init() {
        try{
            Files.createDirectories(rootLocation);
            Files.createDirectories(Path.of(rootLocation + usersFilesPath));
            Files.createDirectories(Path.of(rootLocation + carsFilesPath));
            Files.createDirectories(Path.of(rootLocation + reviewsFilesPath));
        }
        catch (IOException e){
            throw new StorageException("Невозможно создать хранилище ", e);
        }
    }

    public void store(MultipartFile file, String root, String fileName) {
        try{
            if (file.isEmpty()){
                throw new StorageException("Невозможно загрузить пустой файл");
            }
            Path path = Paths.get(rootLocation + root);
            Path destinationFile = path.resolve(
                            Paths.get(fileName))
                    .normalize().toAbsolutePath();
            if (!destinationFile.getParent().equals(Path.of(this.rootLocation.toAbsolutePath() + root))){
                throw new StorageException("Нельзя хранить файл за пределами текущего каталога");
            }
            try(InputStream stream = file.getInputStream()) {
                Files.copy(stream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (IOException e) {
            throw new StorageException("Ошибка сохранения файла ", e);
        }
    }

    public Path load(String file, String root) {
        Path path = Paths.get(rootLocation + root);
        return path.resolve(file);
    }

    public Resource loadAsResource(String filename, String root) {
        try {
            Path file = load(filename, root);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()){
                return resource;
            } else {
                throw new StorageException("Невозможно считать файл " + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageException("Невозможно считать файл " + filename);
        }
    }

    public boolean deleteFile(String filename, String root) {
        File file = new File(rootLocation + root + "/" + filename);
        boolean deleted = file.delete();
        if (!deleted) {
            throw new EntityDeleteException("Не удалось удалить фото " + filename);
        }
        return deleted;
    }
}