package kz.lyalya.sto.util.file_upload_service;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Random;

@Component
public class FilenameService {
    private final Random random = new Random();

    public String createFilename(String entityName, long entityId, String filename) {
        return String.format("%s_%s_%s_%s",entityName, entityId, random.nextInt(), filename);
    }

    public boolean checkFileType(String filename) {
        return filename.endsWith(".png") || filename.endsWith("jpg");
    }
}
