package kz.lyalya.sto.configuration;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.enumeration.UserRole;
import kz.lyalya.sto.common_data.repository.UserRepository;
import kz.lyalya.sto.exception.EntityNotFoundException;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final UserRepository userRepository;
    private final String QUERY = "SET ROLE ";

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public CustomAuthenticationSuccessHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        User user = userRepository.findByEmail(authentication.getName()).orElseThrow(() -> {
            return new EntityNotFoundException("Пользователь " + authentication.getName() + " не найден в системе");
        });
        UserRole role = user.getRoles().get(0).getRole();
        try {
            entityManager.createNativeQuery(QUERY + role.getPostgresRole()).getResultList();
        } catch (GenericJDBCException e) {
            System.out.println(e.getMessage());
        } finally {
            List result = entityManager.createNativeQuery("SELECT current_user").getResultList();

            if (result.size() > 0 && !result.get(0).equals(role.getPostgresRole())) {
                System.out.println("failed set role function");
            }

            System.out.println("perfect");
        }
    }
}