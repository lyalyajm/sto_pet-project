package kz.lyalya.sto.configuration;

import kz.lyalya.sto.common_data.repository.CarPhotoRepository;
import kz.lyalya.sto.common_data.repository.ReviewPhotoRepository;
import kz.lyalya.sto.common_data.repository.UserPhotoRepository;
import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Configuration
@PropertySource("classpath:application.properties")
public class CheckFileInDatabaseConfig {
    private final ThreadPoolExecutor poolExecutor;
    private final UserPhotoRepository userPhotoRepository;
    private final CarPhotoRepository carPhotoRepository;
    private final ReviewPhotoRepository reviewPhotoRepository;
    private final FileUploadService fileUploadService;
    private final String usersFilesPath;
    private final String carsFilesPath;
    private final String reviewsFilesPath;
    private final String root;

    public CheckFileInDatabaseConfig(UserPhotoRepository userPhotoRepository, CarPhotoRepository carPhotoRepository,
                                     ReviewPhotoRepository reviewPhotoRepository,
                                     FileUploadService fileUploadService, @Value("${files_users_upload}") String usersFilesPath,
                                     @Value("${files_cars_upload}") String carsFilesPath,
                                     @Value("${files_reviews_upload}") String reviewsFilesPath,
                                     @Value("${files_upload}") String root) {
        this.poolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        this.userPhotoRepository = userPhotoRepository;
        this.carPhotoRepository = carPhotoRepository;
        this.reviewPhotoRepository = reviewPhotoRepository;
        this.fileUploadService = fileUploadService;
        this.usersFilesPath = usersFilesPath;
        this.carsFilesPath = carsFilesPath;
        this.reviewsFilesPath = reviewsFilesPath;
        this.root = root;
    }

    @Bean
    public boolean deleteUnnecessaryFiles() throws IOException {
        List<Path> userFiles = Files.list(Paths.get(root + usersFilesPath)).collect(Collectors.toList());
        List<Path> carsFiles = Files.list(Paths.get(root + carsFilesPath)).collect(Collectors.toList());
        List<Path> reviewFiles = Files.list(Paths.get(root + reviewsFilesPath)).collect(Collectors.toList());

        userFiles.stream().filter(u -> !userPhotoRepository.existsByFilePath(u.getFileName().toString()))
                .forEach(u -> poolExecutor.execute(new FileDeleter(u.getFileName(), usersFilesPath)));

        carsFiles.stream().filter(c -> !carPhotoRepository.existsByFilePath(c.getFileName().toString()))
                .forEach(c -> poolExecutor.execute(new FileDeleter(c.getFileName(), carsFilesPath)));

        reviewFiles.stream().filter(r -> !reviewPhotoRepository.existsByFilePath(r.getFileName().toString()))
                .forEach(r -> poolExecutor.execute(new FileDeleter(r.getFileName(), reviewsFilesPath)));

        poolExecutor.shutdown();
        return true;
    }

    @AllArgsConstructor
    public class FileDeleter implements Runnable {
        private Path filename;
        private String filesPath;

        @Override
        public void run() {
            fileUploadService.deleteFile(filename.toString(), filesPath);
        }
    }
}
