package kz.lyalya.sto.configuration;

import kz.lyalya.sto.common_service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@Configuration
@EnableMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig {
    private final UserDetailService userDetailService;
    private final CustomAuthenticationSuccessHandler successHandler;
    private final LogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    public SecurityConfig(UserDetailService userDetailService, CustomAuthenticationSuccessHandler successHandler, LogoutSuccessHandler logoutSuccessHandler) {
        this.userDetailService = userDetailService;
        this.successHandler = successHandler;
        this.logoutSuccessHandler = logoutSuccessHandler;
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((authz) -> authz
                        .requestMatchers( "/clients/registration", "/works",
                                        "/masters/all", "/masters/id/**", "/masters/photo/**",
                                        "/reviews/id/**", "/reviews/all", "/reviews/photo/**")
                                .permitAll()

                        .requestMatchers("/clients/id/**", "/clients/all", "/clients/photo/**",
                                        "/cars/all", "/cars/id/**", "/cars/photo/**",
                                        "/requests/id/**", "/requests/all",
                                        "/done-works/id/**", "/done-works/all")
                                .hasAnyAuthority("OWNER", "ADMIN", "MASTER", "CLIENT")

                        .requestMatchers("/clients/{d}/update", "/clients/{d}/update-photo", "/clients/{d}/delete-photo", "/clients/{d}/delete", "/clients/{d}/update-password",
                                "/cars/add", "/cars/update", "/cars/update-photo", "/cars/delete-photo", "/cars/delete",
                                "/requests/add", "/requests/update", "/requests/delete",
                                "/reviews/add", "/reviews/update", "/reviews/update-photo", "/reviews/delete-photo", "/reviews/delete")
                        .hasAnyAuthority( "CLIENT")

                        .requestMatchers("/masters/add").hasAnyAuthority("OWNER", "ADMIN")

                        .requestMatchers("/masters/update", "/masters/update-photo", "/masters/delete-photo",
                                "/masters/delete", "/masters/update-password",
                                "/requests/start", "/requests/finish",
                                "/done-works/add", "/done-works/update", "/done-works/delete")
                        .hasAnyAuthority("MASTER")

                        .anyRequest().authenticated()
                )
                .httpBasic(withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
                .authenticationProvider(authenticationProvider())
                .formLogin(withDefaults())
                .formLogin(f -> f.successHandler(successHandler))
//                .addFilterBefore(new AuthenticationSuccessHandlerImpl(), BasicAuthenticationFilter.class)
                .logout(l -> l.logoutUrl("/logout")
                        .logoutSuccessHandler(logoutSuccessHandler)
                        .logoutSuccessUrl("/login")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID"))
                .sessionManagement(s -> s.sessionFixation().migrateSession()
                                         .maximumSessions(1));
        return http.build();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailService);
        authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder());
        return authenticationProvider;
    }

//    @Bean
//    public HttpSessionEventPublisher httpSessionEventPublisher() {
//        return new HttpSessionEventPublisher();
//    }
}
