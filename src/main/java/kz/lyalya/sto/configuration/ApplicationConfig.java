package kz.lyalya.sto.configuration;

import kz.lyalya.sto.util.file_upload_service.FileUploadService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
    @Bean
    CommandLineRunner init(FileUploadService service) {
        return (args) -> {
            service.init();
        };
    }
}
