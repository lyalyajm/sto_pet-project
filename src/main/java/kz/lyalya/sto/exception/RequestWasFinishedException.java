package kz.lyalya.sto.exception;

public class RequestWasFinishedException extends RuntimeException{
    public RequestWasFinishedException(long id) {
        super("Заявка " + id + " уже была закрыта");
    }
}
