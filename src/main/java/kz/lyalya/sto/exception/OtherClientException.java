package kz.lyalya.sto.exception;

public class OtherClientException extends RuntimeException{
    public OtherClientException(String message) {
        super(message);
    }
}
