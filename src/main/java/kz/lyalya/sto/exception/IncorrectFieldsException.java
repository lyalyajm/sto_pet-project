package kz.lyalya.sto.exception;

public class IncorrectFieldsException extends RuntimeException{
    public IncorrectFieldsException(String message) {
        super(message);
    }
}
