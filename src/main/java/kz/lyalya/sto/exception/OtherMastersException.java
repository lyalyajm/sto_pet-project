package kz.lyalya.sto.exception;

public class OtherMastersException extends RuntimeException{
    public OtherMastersException(String message) {
        super(message);
    }
}
