package kz.lyalya.sto.exception;

public class CarDoesNotBelongsToClientException extends RuntimeException{
    public CarDoesNotBelongsToClientException(String car, String client){
        super("Авто " + car + " не принадлежит клиенту " + client);
    }

    public CarDoesNotBelongsToClientException(String message) {
        super(message);
    }
}
