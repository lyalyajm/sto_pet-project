package kz.lyalya.sto.exception;

public class IncorrectFileFormatException extends RuntimeException{
    public IncorrectFileFormatException() {
        super("Загрузите фото формата jpg или png");
    }
}
