package kz.lyalya.sto.exception;

public class RequestInWorkException extends RuntimeException{
    public RequestInWorkException(String message) {
        super(message);
    }
}
