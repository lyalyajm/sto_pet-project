package kz.lyalya.sto.exception;


public class LoginException extends RuntimeException{
    public LoginException() {
        super("Доступ к данным ограничен");
    }
}
