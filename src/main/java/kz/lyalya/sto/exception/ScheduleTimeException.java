package kz.lyalya.sto.exception;

public class ScheduleTimeException extends RuntimeException {
    public ScheduleTimeException(String message) {
        super(message);
    }
}
