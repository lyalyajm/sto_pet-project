package kz.lyalya.sto.common_data.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserRole {
    OWNER("owner_test"),
    ADMIN("admin_test"),
    MASTER("master_test"),
    CLIENT("client_test");

    public String postgresRole;
}
