package kz.lyalya.sto.common_data.enumeration;

public enum RequestState {
    CREATED, IN_PROCESS, FINISHED, REJECTED
}
