package kz.lyalya.sto.common_data.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum WorkType {
    ENGINE(50000, "Ремонт двигателя"),
    GENERATOR(70000, "Ремонт генератора"),
    STARTER(70000, "Ремонт стартера"),
    INJECTOR(70000, "Ремонт инжектора"),
    COIL(70000, "Ремонт катушек"),
    DISTRIBUTOR(70000, "Ремонт трамблеров"),
    COMMUTATOR(70000, "Ремонт коммутаторов"),
    GASOLINE_PUMP(70000, "Ремонт бензонасосов"),
    COMPUTER(70000, "Ремонт компьютеров"),
    COMPUTER_DIAGNOSTICS(70000, "Компьютерная диагностика"),
    KEY_UNLOCKING(70000, "Разблокировка ключей"),
    BELT(70000, "Замена ремней"),
    SPARK_PLUG(70000, "Замена свечей"),
    FILTERS(70000, "Замена фильтров"),
    START_CAR(70000, "Завести автомобиль"),
    WARMING_UP(70000, "Отогрев замерзшего авто");

    private int price;
    private String onRussian;
}
