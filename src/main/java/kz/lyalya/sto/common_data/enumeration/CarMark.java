package kz.lyalya.sto.common_data.enumeration;

public enum CarMark {
    ЗАЗ, ЗИЛ, Москвич, УАЗ,
    Acura, Adler, Alfa_Romeo, Aston_Martin, Audi,
    Bentley, BMW,
    Cadillac, Callaway, Carbodies, Changan, ChangFeng, Chery, Chevrolet, Chrysler, Citroen, Cizeta,
    Dacia, Daewoo, Dodge, DongFeng, Doninvest, Donkervoort,
    Ferrari, Fiat, Fisker, Ford, Foton,
    Geely, Genesis, Geo, GMC,
    Haval, Honda, HuangHai, Hudson, Hummer, Hyundai,
    Infiniti, Innocenti, Isuzu,
    JAC, Jaguar, Jeep, Jensen, JMC,
    Kia,
    LADA, Lamborghini, Lancia, Land_Rover, Landwind, Lexus, Lifan, Lincoln, Lotus, LTI,
    Maserati, Maybach, Mazda, McLare1, Mercedes_Benz, Mercury, MINI, Mitsubishi, Morris,
    Nissan,
    Opel,
    Peugeot, PGO, Pontiac, Porsche, Premier, Proton, PUCH,
    Ravon, Renault, Rolls_Royce, Ronart, Rover,
    Saab, Shanghai, ShuangHuan, Skoda, Smart, Spyker, SsangYong, Subaru, Suzuki,
    Tesla, Toyota,
    Volkswagen, Volvo, Vortex,
    ZX;
}
