package kz.lyalya.sto.common_data.entity.car;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "cars_photos")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CarPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "file_path")
    @NotNull
    private String filePath;

    @Column(name = "main_photo")
    @NotNull
    @Builder.Default
    private boolean mainPhoto = false;

    @ManyToOne
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Car car;
}

