package kz.lyalya.sto.common_data.entity.schedule;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import kz.lyalya.sto.common_data.entity.request.Request;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "schedule")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Request request;

    @Column(name = "date_time_of_start")
    @NotNull(message = "Необходимо указать дату и время начала работы")
    private LocalDateTime dateTimeOsStart;

    @Column(name = "date_time_of_finish")
    @NotNull(message = "Необходимо указать дату и время окончания работы")
    private LocalDateTime dateTimeOsFinish;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(request, schedule.request) && Objects.equals(dateTimeOsStart, schedule.dateTimeOsStart) && Objects.equals(dateTimeOsFinish, schedule.dateTimeOsFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(request, dateTimeOsStart, dateTimeOsFinish);
    }
}
