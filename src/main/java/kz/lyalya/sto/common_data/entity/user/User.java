package kz.lyalya.sto.common_data.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.validator.constraints.Email;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    @NotNull(message = "Укажите имя")
    @Size(min = 2, max = 30, message = "Диапазон имени 3-30 символов")
    private String name;

    @Column(name = "surname")
    @NotNull(message = "Укажите фамилию")
    @Size(min = 1, max = 30, message = "Диапазон фамилии 1-30 символов")
    private String surname;

    @Column(name = "date_of_birth")
    @NotNull(message = "Укажите дату рождения")
    private LocalDate dateOfBirth;

    @Column(name = "phone_number")
    @NotNull(message = "Укажите сотовый номер")
    @Pattern(regexp = "^((8|\\+374|\\+994|\\+995|\\+375|\\+7|\\+380|\\+38|\\+996|\\+998|\\+993)[\\- ]?)?\\(?\\d{3,5}\\)?[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}(([\\- ]?\\d{1})?[\\- ]?\\d{1})?$")
    private String phoneNumber;

    @Column(name = "phone_number_2")
    @Pattern(regexp = "^((8|\\+374|\\+994|\\+995|\\+375|\\+7|\\+380|\\+38|\\+996|\\+998|\\+993)[\\- ]?)?\\(?\\d{3,5}\\)?[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}[\\- ]?\\d{1}(([\\- ]?\\d{1})?[\\- ]?\\d{1})?$")
    private String phoneNumber2;

    @Column(name = "email")
    @NotNull(message = "Укажите электронную почту")
    private String email;

    @Column(name = "password")
    @NotNull(message = "Введите пароль")
    private String password;

    @Column(name = "is_enabled")
    @Builder.Default
    private boolean isEnabled = true;

    @Column(name = "date_of_creation")
    @Builder.Default
    private LocalDateTime dateOfCreation = LocalDateTime.now();

    @Column(name = "deleted")
    @Builder.Default
    private boolean isDeleted = false;

    @Column(name = "updated")
    @Builder.Default
    private LocalDate updated = LocalDate.now();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    @ToString.Exclude
    private List<UserPhoto> userPhotos = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @Builder.Default
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    @Enumerated(EnumType.STRING)
    private List<Role> roles = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) && Objects.equals(surname, user.surname) && Objects.equals(dateOfBirth, user.dateOfBirth) && Objects.equals(phoneNumber, user.phoneNumber) && Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, dateOfBirth, phoneNumber, email);
    }
}
