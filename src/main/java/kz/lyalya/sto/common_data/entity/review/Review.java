package kz.lyalya.sto.common_data.entity.review;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import lombok.*;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "reviews")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Review implements Checkable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Request request;

    @Column(name = "comment")
    private String comment;

    @Column(name = "rate")
    @NotNull(message = "Оцените работу СТО")
    @Min(value = 1, message = "Минимальная оценка: 1")
    @Max(value = 10, message = "Максимальная оценка: 10")
    private short rate;

    @Column
    @NotNull
    @Builder.Default
    private LocalDate date = LocalDate.now();

    @OneToMany(mappedBy = "review")
    @Builder.Default
    @JsonBackReference
    @ToString.Exclude
    private List<ReviewPhoto> photos = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return rate == review.rate && Objects.equals(request, review.request) && Objects.equals(comment, review.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(request, comment, rate);
    }
}
