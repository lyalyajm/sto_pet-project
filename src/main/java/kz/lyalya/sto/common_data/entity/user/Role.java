package kz.lyalya.sto.common_data.entity.user;

import jakarta.persistence.*;
import kz.lyalya.sto.common_data.enumeration.UserRole;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "roles")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "role")
    @NotNull
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
    private Set<User> users;
}

