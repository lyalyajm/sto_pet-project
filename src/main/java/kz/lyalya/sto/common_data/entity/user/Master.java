package kz.lyalya.sto.common_data.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import kz.lyalya.sto.common_data.entity.request.Request;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "masters")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Master implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private User user;

    @Column(name = "experience")
    @NotNull(message = "Укажите стаж работы мастера")
    private short experience;

    @Column(name = "avg_rate")
    private short avgRate;

    @Column(name = "is_workable")
    @NotNull
    @Builder.Default
    private boolean isWorkable = true;

    @Column(name = "description")
    @NotNull(message = "Введите описание мастера")
    @Size(min = 3, max = 500, message = "Описание должно содержать от 3 до 500 символов")
    private String description;

    @ManyToOne
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private User creator;

    @OneToMany(mappedBy = "master", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    @ToString.Exclude
    private List<Request> requests = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Master master = (Master) o;
        return id == master.id && experience == master.experience && avgRate == master.avgRate && isWorkable == master.isWorkable && Objects.equals(user, master.user) && Objects.equals(description, master.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, experience, avgRate, isWorkable, description);
    }
}
