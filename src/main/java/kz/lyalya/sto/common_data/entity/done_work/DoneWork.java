package kz.lyalya.sto.common_data.entity.done_work;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.enumeration.WorkType;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "done_works")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DoneWork implements Checkable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Request request;

    @Column(name = "work_type")
    @Enumerated
    @NotNull
    private WorkType workType;

    @Column
    private String note;

    @Column(name = "price")
    @NotNull
    private int price;

    @Column
    @Builder.Default
    private LocalDate date = LocalDate.now();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoneWork doneWork = (DoneWork) o;
        return price == doneWork.price && Objects.equals(request, doneWork.request) && workType == doneWork.workType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(request, workType, price);
    }
}