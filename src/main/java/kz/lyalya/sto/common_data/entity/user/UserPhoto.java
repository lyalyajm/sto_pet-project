package kz.lyalya.sto.common_data.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "users_photos")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserPhoto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "file_path")
    @NotNull
    private String filePath;

    @Column(name = "main_photo")
    @NotNull
    @Builder.Default
    private boolean mainPhoto = false;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private User user;
}
