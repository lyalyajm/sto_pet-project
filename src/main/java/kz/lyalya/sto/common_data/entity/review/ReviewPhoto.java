package kz.lyalya.sto.common_data.entity.review;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "reviews_photos")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ReviewPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "file_path")
    @NotNull
    private String filePath;

    @ManyToOne
    @JoinColumn(name = "review_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Review review;
}
