package kz.lyalya.sto.common_data.entity.car;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.enumeration.CarMark;
import kz.lyalya.sto.common_data.enumeration.CarState;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import lombok.*;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cars")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Car implements Checkable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "mark")
    @Enumerated
    @NotNull(message = "Выберите марку машины")
    private CarMark mark;

    @Column(name = "model")
    @NotNull(message = "Укажите модель машины")
    private String model;

    @Column(name = "year_of_produce")
    @NotNull(message = "Укажите год производства")
    @Min(value = 1950, message = "Указан некорректный год производства")
    private int yearOfProduce;

    @Column(name = "state")
    @NotNull
    @Builder.Default
    private CarState state = CarState.NEW;

    @Column(name = "date_of_creation")
    @Builder.Default
    private LocalDateTime dateOfCreation = LocalDateTime.now();

    @Column(name = "deleted")
    @Builder.Default
    private boolean isDeleted = false;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Client owner;

    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER)
    @Builder.Default
    @JsonBackReference
    @ToString.Exclude
    private List<CarPhoto> photos = new ArrayList<>();

    @OneToMany(mappedBy = "car", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    @ToString.Exclude
    private List<Request> requests = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return yearOfProduce == car.yearOfProduce && mark == car.mark && Objects.equals(model, car.model) && Objects.equals(owner, car.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mark, model, yearOfProduce, owner);
    }
}
