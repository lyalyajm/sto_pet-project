package kz.lyalya.sto.common_data.entity.request;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.common_service.intf.entity_belongs_checker.Checkable;
import lombok.*;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "requests")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Request implements Checkable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Client client;

    @ManyToOne
    @JoinColumn(name = "master_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Master master;

    @ManyToOne
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    @JsonBackReference
    @ToString.Exclude
    private Car car;

    @Column(name = "problem_description")
    @NotNull(message = "Опишите проблему, можно буквально в двух словах")
    @Size(min = 3, max = 500, message = "Описание проблемы в рамках от 3 до 500 символов")
    private String problemDescription;

    @Column(name = "date_of_creation")
    @Builder.Default
    private LocalDateTime dateOfCreation = LocalDateTime.now();

    @Column(name = "date_of_start")
    private LocalDate dateOfStart;

    @Column(name = "date_of_finish")
    private LocalDate dateOfFinish;

    @Column(name = "deleted")
    @Builder.Default
    private boolean isDeleted = false;

    @Column(name = "state")
    @NotNull
    @Enumerated
    @Builder.Default
    private RequestState state = RequestState.CREATED;

    @Column(name = "final_price")
    private Integer price;

    @Column(name = "current_price")
    @NotNull
    @Min(0)
    @Builder.Default
    private int currentPrice = 0;

    @OneToOne(mappedBy = "request", fetch = FetchType.LAZY)
    @JsonBackReference
    @ToString.Exclude
    private Review review;

    @OneToMany(mappedBy = "request", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    @ToString.Exclude
    private List<DoneWork> doneWorks = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(client, request.client) && Objects.equals(master, request.master) && Objects.equals(car, request.car) && Objects.equals(dateOfCreation, request.dateOfCreation) && Objects.equals(dateOfStart, request.dateOfStart) && Objects.equals(dateOfFinish, request.dateOfFinish) && state == request.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, master, car, dateOfCreation, dateOfStart, dateOfFinish, state);
    }
}
