package kz.lyalya.sto.common_data.pagination_specification.done_work;

import jakarta.persistence.criteria.*;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.enumeration.WorkType;
import kz.lyalya.sto.dto.done_work.DoneWorkSearchDTO;
import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SortingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class DoneWorkSpecification implements ModelSpecification<DoneWork, DoneWorkSearchDTO> {

    @Override
    public Specification<DoneWork> createSpecification(DoneWorkSearchDTO filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (filter.getMasterId() > 0) {
                    Join<DoneWork, Request> requestJoin = root.join("request");
                    Join<Request, Master> masterJoin = requestJoin.join("master");
                    predicates.add(criteriaBuilder.equal(masterJoin.get("id"), filter.getMasterId()));
                }
                if (filter.getClientId() > 0) {
                    Join<DoneWork, Request> requestJoin = root.join("request");
                    Join<Request, Client> clientJoin = requestJoin.join("client");
                    predicates.add(criteriaBuilder.equal(clientJoin.get("id"), filter.getClientId()));
                }
                if (filter.getCarId() > 0) {
                    Join<DoneWork, Request> requestJoin = root.join("request");
                    Join<Request, Car> carJoin = requestJoin.join("car");
                    predicates.add(criteriaBuilder.equal(carJoin.get("id"), filter.getCarId()));
                }
                if (!isEmpty(filter.getWorkType())) {
                    predicates.add(criteriaBuilder.equal(root.get("workType").as(WorkType.class), WorkType.valueOf(filter.getWorkType())));
                }
                if (filter.getFrom() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("date").as(LocalDate.class), filter.getFrom()));
                }
                if (filter.getTo() != null) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("date").as(LocalDate.class), filter.getTo()));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public void sorting(Root<DoneWork> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder, SortingModel sorting) {
        ModelSpecification.super.sorting(root, criteriaQuery, criteriaBuilder, sorting);
    }
}
