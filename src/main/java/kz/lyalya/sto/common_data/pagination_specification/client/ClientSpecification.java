package kz.lyalya.sto.common_data.pagination_specification.client;

import jakarta.persistence.criteria.*;
import kz.lyalya.sto.dto.client.ClientSearchDTO;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SortingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class ClientSpecification implements ModelSpecification<Client, ClientSearchDTO> {

    public static Specification<Client> joinTest(String input) {
        return new Specification<Client>() {
            public Predicate toPredicate(Root<Client> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Join<Client, User> userProd = root.join("user");
                return cb.equal(userProd.get("name"), input);
            }
        };
    }

    @Override
    public Specification<Client> createSpecification(ClientSearchDTO filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getName())) {
                    Join<Client,User> personProd = root.join("user");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("name")), "%" + filter.getName().toLowerCase() + "%"));
                }
                if (!isEmpty(filter.getSurname())) {
                    Join<Client,User> personProd = root.join("user");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("surname")), "%" + filter.getSurname().toLowerCase() + "%"));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
