package kz.lyalya.sto.common_data.pagination_specification.master;

import jakarta.persistence.criteria.*;
import kz.lyalya.sto.dto.master.MasterSearchDTO;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SortingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class MasterSpecification implements ModelSpecification<Master, MasterSearchDTO> {

    public static Specification<Master> joinTest(String input) {
        return new Specification<Master>() {
            public Predicate toPredicate(Root<Master> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Join<Master, User> userProd = root.join("user");
                return cb.equal(userProd.get("name"), input);
            }
        };
    }

    @Override
    public Specification<Master> createSpecification(MasterSearchDTO filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getName())) {
                    Join<Master,User> personProd = root.join("user");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("name")), "%" + filter.getName().toLowerCase() + "%"));
                }
                if (!isEmpty(filter.getSurname())) {
                    Join<Master,User> personProd = root.join("user");
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(personProd.get("surname")), "%" + filter.getSurname().toLowerCase() + "%"));
                }
                if (filter.getRate() > 0) {
                    predicates.add(criteriaBuilder.equal(root.get("avgRate"), filter.getRate()));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
