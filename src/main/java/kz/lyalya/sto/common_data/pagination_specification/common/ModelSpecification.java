package kz.lyalya.sto.common_data.pagination_specification.common;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

public interface ModelSpecification<T, R extends FilterModel> {

    Specification<T> createSpecification(R filter, SortingModel sorting);

    default void sorting(Root<T> root,
                         CriteriaQuery<?> criteriaQuery,
                         CriteriaBuilder criteriaBuilder,
                         SortingModel sorting) {
        if (sorting != null) {
            if (!Sort.Direction.ASC.equals(sorting.getOrder())) {
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sorting.getField())));
            } else {
                criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sorting.getField())));
            }
        }
    }
}
