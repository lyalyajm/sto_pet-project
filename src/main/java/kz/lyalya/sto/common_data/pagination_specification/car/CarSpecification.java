package kz.lyalya.sto.common_data.pagination_specification.car;

import jakarta.persistence.criteria.*;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.enumeration.CarMark;
import kz.lyalya.sto.dto.car.CarSearchDTO;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SortingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class CarSpecification implements ModelSpecification<Car, CarSearchDTO> {

    @Override
    public Specification<Car> createSpecification(CarSearchDTO filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getMark())) {
                    predicates.add(criteriaBuilder.equal(root.get("mark").as(CarMark.class), CarMark.valueOf(filter.getMark())));
                }
                if (!isEmpty(filter.getModel())) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("model")), "%" + filter.getModel().toLowerCase() + "%"));
                }
                if (filter.getClientId() > 0) {
                    Join<Car, Client> clientJoin = root.join("client");
                    predicates.add(criteriaBuilder.equal(clientJoin.get("id"), filter.getClientId()));
                }
                if (filter.getYearOfProduce() > 1950) {
                    predicates.add(criteriaBuilder.equal(root.get("yearOfProduce"), filter.getYearOfProduce()));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
