package kz.lyalya.sto.common_data.pagination_specification.request;

import jakarta.persistence.criteria.*;
import kz.lyalya.sto.common_data.enumeration.CarMark;
import kz.lyalya.sto.common_data.enumeration.RequestState;
import kz.lyalya.sto.dto.request.RequestSearchDTO;
import kz.lyalya.sto.common_data.entity.car.Car;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SortingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class RequestSpecification implements ModelSpecification<Request, RequestSearchDTO> {

    //https://stackoverflow.com/questions/66451136/how-to-write-a-spring-boot-jpa-specification-joining-multiple-tables
    @Override
    public Specification<Request> createSpecification(RequestSearchDTO filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (filter.getFinished() != null && filter.getFinished()) {
                    predicates.add(criteriaBuilder.equal(root.get("state").as(RequestState.class), RequestState.FINISHED));
                }
                if (filter.getFinished() != null && !filter.getFinished()) {
                    predicates.add(criteriaBuilder.notEqual(root.get("state").as(RequestState.class), RequestState.FINISHED));
                }
                if (!isEmpty(filter.getClientName())) {
                    Join<Request, User> clientJoin = root.join("client");
                    Join<Client, User> clientUserJoin = clientJoin.join("user");
                    predicates.add(criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.lower(clientUserJoin.get("name")), "%" + filter.getClientName().toLowerCase() + "%"),
                            criteriaBuilder.like(criteriaBuilder.lower(clientUserJoin.get("surname")), "%" + filter.getClientName().toLowerCase() + "%")));
                }
                if (filter.getClientId() > 0) {
                    Join<Request, User> clientJoin = root.join("client");
                    predicates.add(criteriaBuilder.equal(clientJoin.get("id"), filter.getClientId()));
                }
                if (!isEmpty(filter.getMasterName())) {
                    Join<Request, Master> masterJoin = root.join("master");
                    Join<Master, User> masterUserJoin = masterJoin.join("user");
                    predicates.add(criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.lower(masterUserJoin.get("name")), "%" + filter.getMasterName().toLowerCase() + "%"),
                            criteriaBuilder.like(criteriaBuilder.lower(masterUserJoin.get("surname")), "%" + filter.getMasterName().toLowerCase() + "%")));
                }
                if (filter.getMasterId() > 0) {
                    Join<Request, Master> masterJoin = root.join("master");
                    predicates.add(criteriaBuilder.equal(masterJoin.get("id"), filter.getMasterId()));
                }
                if (filter.getFrom() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("dateOfFinish").as(LocalDate.class), filter.getFrom()));
                }
                if (filter.getTo() != null) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("dateOfFinish").as(LocalDate.class), filter.getTo()));
                }
                if (filter.getRateFrom() > 0) {
                    Join<Request, Review> reviewJoin = root.join("review");
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(reviewJoin.get("rate"), filter.getRateFrom()));
                }
                if (filter.getRateTo() > 0) {
                    Join<Request, Review> reviewJoin = root.join("review");
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(reviewJoin.get("rate"), filter.getRateTo()));
                }
                if (filter.getPriceFrom() > 0) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), filter.getPriceFrom()));
                }
                if (filter.getPriceTo() > 0) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), filter.getPriceTo()));
                }
                if (!isEmpty(filter.getCarMark())) {
                    Join<Request, Car> carJoin = root.join("car");
                    predicates.add(criteriaBuilder.equal(carJoin.get("mark").as(CarMark.class), CarMark.valueOf(filter.getCarMark())));
                }

            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}

