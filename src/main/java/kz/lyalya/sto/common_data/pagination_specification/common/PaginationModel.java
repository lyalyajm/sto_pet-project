package kz.lyalya.sto.common_data.pagination_specification.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;

@Getter
@Setter
public class PaginationModel {
    private int page = 0;
    private int size = 15;

    public PageRequest getPageRequest() {
       return PageRequest.of(page, size);
    }
}
