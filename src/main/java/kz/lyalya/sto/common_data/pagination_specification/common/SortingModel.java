package kz.lyalya.sto.common_data.pagination_specification.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Getter
@Setter
public class SortingModel {
    private String field = "id";
    private Sort.Direction order = ASC;
}
