package kz.lyalya.sto.common_data.pagination_specification.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SearchModel<T extends FilterModel> {
    private T filter;
    private SortingModel sort;
    private PaginationModel pagination;
}
