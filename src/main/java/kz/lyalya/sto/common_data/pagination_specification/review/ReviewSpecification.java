package kz.lyalya.sto.common_data.pagination_specification.review;

import jakarta.persistence.criteria.*;
import kz.lyalya.sto.dto.review.ReviewSearchDTO;
import kz.lyalya.sto.common_data.entity.request.Request;
import kz.lyalya.sto.common_data.entity.review.Review;
import kz.lyalya.sto.common_data.entity.user.Client;
import kz.lyalya.sto.common_data.entity.user.Master;
import kz.lyalya.sto.common_data.entity.user.User;
import kz.lyalya.sto.common_data.pagination_specification.common.ModelSpecification;
import kz.lyalya.sto.common_data.pagination_specification.common.SortingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isEmpty;

@Component
@RequiredArgsConstructor
public class ReviewSpecification implements ModelSpecification<Review, ReviewSearchDTO> {

    @Override
    public Specification<Review> createSpecification(ReviewSearchDTO filter, SortingModel sorting) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (!isEmpty(filter.getClientName())) {
                    Join<Review, Request> requestJoin = root.join("request");
                    Join<Request, Client> clientJoin = requestJoin.join("client");
                    Join<Client, User> userJoin = clientJoin.join("user");
                    predicates.add(criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.lower(userJoin.get("name")), "%" + filter.getClientName().toLowerCase() + "%"),
                            criteriaBuilder.like(criteriaBuilder.lower(userJoin.get("surname")), "%" + filter.getClientName().toLowerCase() + "%")));
                }
                if (filter.getClientId() > 0) {
                    Join<Review, Request> requestJoin = root.join("request");
                    Join<Request, Client> clientJoin = requestJoin.join("client");
                    predicates.add(criteriaBuilder.equal(clientJoin.get("id"), filter.getClientId()));
                }
                if (!isEmpty(filter.getMasterName())) {
                    Join<Review, Request> requestJoin = root.join("request");
                    Join<Request, Master> masterJoin = requestJoin.join("master");
                    Join<Master, User> userJoin = masterJoin.join("user");
                    predicates.add(criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.lower(userJoin.get("name")), "%" + filter.getMasterName().toLowerCase() + "%"),
                            criteriaBuilder.like(criteriaBuilder.lower(userJoin.get("surname")), "%" + filter.getMasterName().toLowerCase() + "%")));
                }
                if (filter.getMasterId() > 0) {
                    Join<Review, Request> requestJoin = root.join("request");
                    Join<Request, Master> masterJoin = requestJoin.join("master");
                    predicates.add(criteriaBuilder.equal(masterJoin.get("id"), filter.getMasterId()));
                }
                if (filter.getFrom() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("date"), filter.getFrom()));
                }
                if (filter.getTo() != null) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("date"),  filter.getTo()));
                }
                if (filter.getRateFrom() > 0) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("rate"),  filter.getRateFrom()));
                }
                if (filter.getRateTo() > 0) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("rate"), filter.getRateTo()));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}

