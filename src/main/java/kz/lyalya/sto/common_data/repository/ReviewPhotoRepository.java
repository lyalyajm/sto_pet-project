package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.review.ReviewPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReviewPhotoRepository extends JpaRepository<ReviewPhoto, Long> {
    boolean existsByFilePath(String filePath);
    Optional<ReviewPhoto> findByFilePathAndReviewId(String filePath, long reviewId);
    boolean existsByFilePathAndReviewId(String filePath, long reviewId);
}
