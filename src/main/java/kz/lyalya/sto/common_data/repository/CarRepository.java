package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.car.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long>, JpaSpecificationExecutor<Car> {
    void deleteAllByOwnerId(long ownerId);
}
