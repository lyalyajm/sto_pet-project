package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.user.Master;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MasterRepository extends JpaRepository<Master, Long>, JpaSpecificationExecutor<Master> {
    Optional<Master> findByUserEmail(String email);
}
