package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.car.CarPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarPhotoRepository extends JpaRepository<CarPhoto, Long> {
    boolean existsByFilePath(String path);
    Optional<CarPhoto> findByFilePathAndCarId(String filePath, long carId);
    boolean existsByFilePathAndCarId(String filePath, long carId);
    Optional<CarPhoto> findByCarIdAndMainPhoto(long carId, boolean isMainPhoto);
    List<CarPhoto> findByCarId(long carId);
}
