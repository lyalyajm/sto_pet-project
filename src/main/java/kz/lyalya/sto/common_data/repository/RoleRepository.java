package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.user.Role;
import kz.lyalya.sto.common_data.enumeration.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRole(UserRole role);
}
