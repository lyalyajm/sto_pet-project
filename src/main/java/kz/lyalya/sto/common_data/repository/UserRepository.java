package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
//
//    @Query(nativeQuery = true, value = "SET ROLE :role")
//    void setPostgresUserRole(@Param("role") String role);
}
