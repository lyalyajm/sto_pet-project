package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.user.UserPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPhotoRepository extends JpaRepository<UserPhoto, Long> {
    boolean existsByFilePath(String path);
    Optional<UserPhoto> findByFilePathAndUserId(String filePath, long userId);
    boolean existsByFilePathAndUserId(String filePath, long userId);
    Optional<UserPhoto> findByUserIdAndMainPhoto(long userId, boolean isMainPhoto);
    List<UserPhoto> findByUserId(long userId);
}
