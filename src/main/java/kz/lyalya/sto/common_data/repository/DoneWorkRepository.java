package kz.lyalya.sto.common_data.repository;

import kz.lyalya.sto.common_data.entity.done_work.DoneWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DoneWorkRepository extends JpaRepository<DoneWork, Long>, JpaSpecificationExecutor<DoneWork> {
}
