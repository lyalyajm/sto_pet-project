REVOKE CREATE ON SCHEMA public FROM public;
REVOKE ALL ON DATABASE sto FROM public;

DO
$$
    BEGIN
        IF NOT EXISTS(SELECT * FROM pg_roles WHERE rolname = 'sto_owner') THEN
            CREATE ROLE sto_owner;
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_user WHERE usename = 'owner_test') THEN
            CREATE USER owner_test WITH PASSWORD '1234';
        END IF;

        IF NOT EXISTS(SELECT * FROM pg_roles WHERE rolname = 'sto_admin') THEN
            CREATE ROLE sto_admin;
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_user WHERE usename = 'admin_test') THEN
            CREATE USER admin_test WITH PASSWORD '1234';
        END IF;

        IF NOT EXISTS(SELECT * FROM pg_roles WHERE rolname = 'sto_master') THEN
            CREATE ROLE sto_master;
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_user WHERE usename = 'master_test') THEN
            CREATE USER master_test WITH PASSWORD '1234';
        END IF;

        IF NOT EXISTS(SELECT * FROM pg_roles WHERE rolname = 'sto_client') THEN
            CREATE ROLE sto_client;
        END IF;
        IF NOT EXISTS(SELECT * FROM pg_user WHERE usename = 'client_test') THEN
            CREATE USER client_test WITH PASSWORD '1234';
        END IF;
    END
$$ LANGUAGE plpgsql;

--roles
-- CREATE ROLE sto_owner;
-- CREATE ROLE sto_admin;
-- CREATE ROLE sto_master;
-- CREATE ROLE sto_client;

--users
-- CREATE USER owner_test WITH PASSWORD '1234';
-- CREATE USER admin_test WITH PASSWORD '1234';
-- CREATE USER master_test WITH PASSWORD '1234';
-- CREATE USER client_test WITH PASSWORD '1234';

--giving roles to users
GRANT sto_owner TO owner_test;
GRANT sto_admin TO admin_test;
GRANT sto_master TO master_test;
GRANT sto_client TO client_test;

--owner's grants
GRANT CONNECT ON DATABASE sto TO sto_owner;
GRANT CREATE ON DATABASE sto TO sto_owner;
GRANT USAGE ON SCHEMA public TO sto_owner;
GRANT CREATE ON SCHEMA public TO sto_owner;
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES, TRIGGER ON ALL TABLES
    IN SCHEMA public
    TO sto_owner;

--admin's grants
GRANT CONNECT ON DATABASE sto TO sto_admin;
GRANT USAGE ON SCHEMA public TO sto_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE
    public.masters,
    public.users,
    public.users_photos,
    public.schedule
    TO sto_admin;
GRANT SELECT ON TABLE
    public.clients,
    public.cars,
    public.cars_photos,
    public.requests,
    public.done_works,
    public.reviews,
    public.reviews_photos,
    public.users_roles
    TO sto_admin;

--master's grants
GRANT CONNECT ON DATABASE sto TO sto_master;
GRANT USAGE ON SCHEMA public TO sto_master;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE
    public.masters,
    public.users,
    public.users_photos,
    public.schedule,
    public.done_works,
    public.requests
    TO sto_master;

GRANT SELECT ON TABLE
    public.clients,
    public.cars,
    public.cars_photos,
    public.reviews,
    public.reviews_photos
    TO sto_admin;

--client's grants
GRANT CONNECT ON DATABASE sto TO sto_client;
GRANT USAGE ON SCHEMA public TO sto_client;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE
    public.clients,
    public.users,
    public.users_photos,
    public.cars,
    public.cars_photos,
    public.requests,
    public.reviews,
    public.reviews_photos
    TO sto_master;