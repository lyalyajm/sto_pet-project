CREATE INDEX IF NOT EXISTS idx_sto_users_name_surname ON public.users(name, surname);
CREATE INDEX IF NOT EXISTS idx_sto_cars_mark_model ON public.cars(mark, model);
CREATE INDEX IF NOT EXISTS idx_sto_done_works_date ON public.done_works(date);