--triggers

--count request's current price after insert into done_works
CREATE OR REPLACE FUNCTION count_request_current_price_insert() RETURNS TRIGGER AS $$
BEGIN
	UPDATE requests
	SET current_price = current_price + NEW.price
	WHERE id = NEW.request_id;
	RETURN NEW;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_request_current_price_insert ON done_works;
CREATE TRIGGER update_request_current_price_insert AFTER INSERT ON done_works
	FOR EACH ROW EXECUTE PROCEDURE count_request_current_price_insert();


--count request's current price after delete on done_works
CREATE OR REPLACE FUNCTION count_request_current_price_delete() RETURNS TRIGGER AS $$
BEGIN
UPDATE requests
SET current_price = current_price - OLD.price
WHERE id = OLD.request_id;
RETURN OLD;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_request_current_price_delete ON done_works;
CREATE TRIGGER update_request_current_price_delete AFTER DELETE ON done_works
    FOR EACH ROW EXECUTE PROCEDURE count_request_current_price_delete();


--count request's current price after update into done_works
CREATE OR REPLACE FUNCTION count_request_current_price_update() RETURNS TRIGGER AS $$
BEGIN
	IF NEW.price <> OLD.price THEN
		UPDATE requests
		SET current_price = current_price + NEW.price
		WHERE id = NEW.request_id;

		UPDATE requests
		SET current_price = current_price - OLD.price
		WHERE id = OLD.request_id;
	END IF;
	RETURN OLD;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_request_current_price_update ON done_works;
CREATE TRIGGER update_request_current_price_update AFTER UPDATE ON done_works
	FOR EACH ROW EXECUTE PROCEDURE count_request_current_price_update();
