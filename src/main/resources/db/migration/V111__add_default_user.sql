INSERT INTO users(name, surname, date_of_birth, phone_number, email, password, updated, date_of_creation)
VALUES
('owner', 'owner', '2024-04-05', '+77777777777', 'owner@mail.ru', '$2a$10$Ng9wKWsI47nYZ/6XXckDgeXCvI.tLESmoOBXMCL0IRnYcDG7uIJXy', now(), now()),
('admin', 'admin', '2024-04-05', '+77777777777', 'admin@mail.ru', '$2a$10$ATdOcpe7rX8ZLFNEjoTRtel6iL4lmBB/IjcJLGe/XgO6WuOtcXKd2', now(), now()),
('master', 'master', '2024-04-05', '+77777777777', 'master@mail.ru', '$2a$10$xnY80J0y3vJZLO5XYBpvheBQ4TpspE9Ufeim.8DluVo6zWpUwfG8i', now(), now()),
('client', 'client', '2024-04-05', '+77777777777', 'client@mail.ru', '$2a$10$VLrZ2tFgwK4cZx/TnTPDoezEe52Hz8OSei.MFBkDeAVLQYdiA8C3m', now(), now());

INSERT INTO users_roles (user_id, role_id)
VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);