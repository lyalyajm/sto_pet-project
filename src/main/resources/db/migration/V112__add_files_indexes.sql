CREATE INDEX IF NOT EXISTS idx_users_photo_filepath_user_id ON public.users_photos(file_path, user_id);
CREATE INDEX IF NOT EXISTS idx_cars_photo_filepath_car_id ON public.cars_photos(file_path, car_id);
CREATE INDEX IF NOT EXISTS idx_reviews_photo_filepath_car_id ON public.reviews_photos(file_path, review_id);