--clients table
CREATE TABLE IF NOT EXISTS clients(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    user_id bigint NOT NULL,

    CONSTRAINT PK_client_id PRIMARY KEY(id),
    CONSTRAINT FK_client_user FOREIGN KEY(user_id) REFERENCES users
);

--masters table
CREATE TABLE IF NOT EXISTS masters(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    user_id bigint NOT NULL,
    experience smallint NOT NULL DEFAULT 0,
    avg_rate smallint NOT NULL DEFAULT 0,
    description varchar(500) NOT NULL,
    is_workable boolean DEFAULT true,
    creator_id bigint NOT NULL,

    CONSTRAINT PK_master_id PRIMARY KEY(id),
    CONSTRAINT FK_master_user FOREIGN KEY(user_id) REFERENCES users,
    CONSTRAINT FK_master_creator FOREIGN KEY(creator_id) REFERENCES users,
    CONSTRAINT check_master_experience CHECK(experience >= 0)
);