-- TRUNCATE flyway_schema_history;

--users table
CREATE TABLE IF NOT EXISTS users(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    name varchar(128) NOT NULL,
    surname varchar(128) NOT NULL,
    date_of_birth date NOT NULL,
    phone_number varchar(20) NOT NULL,
    phone_number_2 varchar(20),
    email varchar(70) NOT NULL,
    password varchar(200) NOT NULL,
    updated timestamp NOT NULL,
    is_enabled boolean DEFAULT true,
    date_of_creation timestamp NOT NULL,
    deleted boolean DEFAULT false,

    CONSTRAINT PK_user_id PRIMARY KEY(id),
    CONSTRAINT unique_email UNIQUE(email),
    CONSTRAINT check_user_email_format CHECK (email ~* '^[A-Za-z0-9._+%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
);


--users' photos table
CREATE TABLE IF NOT EXISTS users_photos(
	id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	file_path text NOT NULL,
	main_photo boolean NOT NULL DEFAULT false,
	user_id bigint NOT NULL,

	CONSTRAINT PK_users_photo_id PRIMARY KEY(id),
	CONSTRAINT FK_users_photo_user FOREIGN KEY(user_id) REFERENCES users
);


