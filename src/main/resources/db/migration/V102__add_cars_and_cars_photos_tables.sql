--cars table
CREATE TABLE IF NOT EXISTS cars(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    mark varchar(128) NOT NULL,
    model varchar(128) NOT NULL,
    year_of_produce smallint NOT NULL,
    state varchar(30) NOT NULL,
    deleted boolean NOT NULL DEFAULT false,
    date_of_creation timestamp NOT NULL,
    client_id bigint NOT NULL,

    CONSTRAINT PK_car_id PRIMARY KEY(id),
    CONSTRAINT FK_car_client FOREIGN KEY(client_id) REFERENCES clients,
    CONSTRAINT check_car_year_of_produce CHECK(year_of_produce >= 1950)
);

--cars' photos table
CREATE TABLE IF NOT EXISTS cars_photos(
	id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	file_path text NOT NULL,
	main_photo boolean NOT NULL DEFAULT false,
	car_id bigint NOT NULL,

	CONSTRAINT PK_cars_photo_id PRIMARY KEY(id),
	CONSTRAINT FK_cars_photo_car FOREIGN KEY(car_id) REFERENCES cars
);
