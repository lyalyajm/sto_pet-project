--roles table
CREATE TABLE IF NOT EXISTS roles(
    id smallint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    role varchar(30) NOT NULL,

    CONSTRAINT PK_role_id PRIMARY KEY(id)
);

--add default roles
INSERT INTO roles(role)
VALUES
('OWNER'),
('ADMIN'),
('MASTER'),
('CLIENT');

--users and roles table
CREATE TABLE IF NOT EXISTS users_roles(
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,

    CONSTRAINT FK_user_role_id FOREIGN KEY(user_id) REFERENCES users,
    CONSTRAINT FK_role_user_id FOREIGN KEY(role_id) REFERENCES roles,
    CONSTRAINT PK_users_roles PRIMARY KEY (user_id, role_id)
);