--schedule table
CREATE TABLE IF NOT EXISTS schedule(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    request_id bigint NOT NULL,
    date_time_of_start timestamp NOT NULL,
    date_time_of_finish timestamp NOT NULL,

    CONSTRAINT PK_schedule_id PRIMARY KEY (id),
    CONSTRAINT FK_schedule_request FOREIGN KEY(request_id) REFERENCES requests,
    CONSTRAINT check_date_time_of_start_and_finish CHECK (date_time_of_finish > schedule.date_time_of_finish)
);