--reviews table
CREATE TABLE IF NOT EXISTS reviews(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    request_id bigint NOT NULL,
    comment text NOT NULL,
    rate smallint NOT NULL,
    date date NOT NULL,

    CONSTRAINT PK_review_id PRIMARY KEY(id),
    CONSTRAINT FK_review_request FOREIGN KEY(request_id) REFERENCES requests,
    CONSTRAINT check_review_rate CHECK(rate >= 1 AND rate <= 10)
);


--reviews' photos table
CREATE TABLE IF NOT EXISTS reviews_photos(
	id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	file_path text NOT NULL,
	review_id bigint NOT NULL,

	CONSTRAINT PK_reviews_photo_id PRIMARY KEY(id),
	CONSTRAINT FK_reviews_photo_review FOREIGN KEY(review_id) REFERENCES reviews
);
