--requests table
CREATE TABLE IF NOT EXISTS requests(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    client_id bigint NOT NULL,
    master_id bigint NOT NULL,
    car_id bigint NOT NULL,
    problem_description text NOT NULL,
    date_of_creation timestamp NOT NULL,
    date_of_start date,
    date_of_finish date,
    deleted boolean NOT NULL DEFAULT false,
    state varchar(40) NOT NULL,
    final_price int,
    current_price int NOT NULL,

    CONSTRAINT PK_request_id PRIMARY KEY(id),
    CONSTRAINT FK_request_client FOREIGN KEY(client_id) REFERENCES clients,
    CONSTRAINT FK_request_master FOREIGN KEY(master_id) REFERENCES masters,
    CONSTRAINT FK_request_car FOREIGN KEY(car_id) REFERENCES cars,
    CONSTRAINT check_request_current_price CHECK (current_price >= 0)
);

--done works table
CREATE TABLE IF NOT EXISTS done_works(
    id bigint GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    request_id bigint NOT NULL,
    work_type text NOT NULL,
    note text,
    price int NOT NULL,
    date date NOT NULL,

    CONSTRAINT PK_done_work_id PRIMARY KEY(id),
    CONSTRAINT FK_done_work_request FOREIGN KEY(request_id) REFERENCES requests,
    CONSTRAINT check_done_work_price CHECK(price >= 0)
);