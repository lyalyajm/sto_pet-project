--triggers

--count master's rate after insert into reviews
CREATE OR REPLACE FUNCTION count_master_avg_rate_insert() RETURNS TRIGGER AS $$
DECLARE
    rate_sum int;
    rate_amount int;
    added_master_id bigint;
    result smallint;
BEGIN
    SELECT master_id INTO added_master_id FROM requests AS r WHERE r.id = NEW.request_id;

    SELECT COUNT(*) INTO rate_amount
    FROM reviews
        JOIN requests AS r on reviews.request_id = r.id
    WHERE r.master_id = added_master_id;

    SELECT SUM(rate) INTO rate_sum
    FROM reviews
         JOIN requests AS r on reviews.request_id = r.id
    WHERE r.master_id = added_master_id;

    SELECT rate_sum / rate_amount INTO result;

    UPDATE masters
	SET avg_rate = result
    WHERE id = added_master_id;

	RETURN NEW;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_master_rate_insert ON reviews;
CREATE TRIGGER update_master_rate_insert AFTER INSERT ON reviews
	FOR EACH ROW EXECUTE PROCEDURE count_master_avg_rate_insert();


--count master's rate after delete from reviews
CREATE OR REPLACE FUNCTION count_master_avg_rate_delete() RETURNS TRIGGER AS $$
DECLARE
    rate_sum int;
    rate_amount int;
    added_master_id bigint;
    result smallint;
BEGIN
    SELECT master_id INTO added_master_id FROM requests AS r WHERE r.id = OLD.request_id;

    SELECT COUNT(*) INTO rate_amount
    FROM reviews
             JOIN requests AS r on reviews.request_id = r.id
    WHERE r.master_id = added_master_id;

    SELECT SUM(rate) INTO rate_sum
    FROM reviews
             JOIN requests AS r on reviews.request_id = r.id
    WHERE r.master_id = added_master_id;

    SELECT rate_sum / rate_amount INTO result;

    UPDATE masters
    SET avg_rate = result
    WHERE id = added_master_id;

    RETURN OLD;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_master_avg_rate_delete ON reviews;
CREATE TRIGGER update_master_avg_rate_delete AFTER DELETE ON reviews
    FOR EACH ROW EXECUTE PROCEDURE count_master_avg_rate_delete();


--count request's current price after update into done_works
CREATE OR REPLACE FUNCTION count_master_avg_rate_update() RETURNS TRIGGER AS $$
DECLARE
    rate_sum int;
    rate_amount int;
    added_master_id bigint;
    result smallint;
BEGIN
	IF NEW.rate <> OLD.rate THEN
        SELECT master_id INTO added_master_id FROM requests AS r WHERE r.id = NEW.request_id;

        SELECT COUNT(*) INTO rate_amount
        FROM reviews
                 JOIN requests AS r on reviews.request_id = r.id
        WHERE r.master_id = added_master_id;

        SELECT SUM(rate) INTO rate_sum
        FROM reviews
                 JOIN requests AS r on reviews.request_id = r.id
        WHERE r.master_id = added_master_id;

        SELECT rate_sum / rate_amount INTO result;

        UPDATE masters
        SET avg_rate = result
        WHERE id = added_master_id;

	END IF;
	RETURN OLD;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_master_avg_rate_update ON reviews;
CREATE TRIGGER update_master_avg_rate_update AFTER UPDATE ON reviews
	FOR EACH ROW EXECUTE PROCEDURE count_master_avg_rate_update();
